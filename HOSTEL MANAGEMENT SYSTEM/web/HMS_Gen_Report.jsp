<%-- 
    Document   : HMS_Gen_Report
    Created on : 25-Aug-2015, 03:13:37
    Author     : user
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Picture1.png"/>
        <title>HMS Reports</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
        
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
           
       $(function() {
$( "#datepicker" ).datepicker();
});

 function load_years(){    

var v= document.getElementById("Par_code").value;
  var year_label = document.getElementById("year_label");
  var year = document.getElementById("year");
  var year_clear = document.getElementById("year_clear");
  var year_gen_gen = document.getElementById("year_gen_gen");
  var date_range_table = document.getElementById("date_range_table");
  var Start_date_label = document.getElementById("Start_date_label");
  var start_date = document.getElementById("start_date");
   var end_date_label = document.getElementById("end_date_label");
  var end_date = document.getElementById("end_date");
  var date_clear = document.getElementById("date_clear");
  var date_gen = document.getElementById("date_gen"); 
  var parcel_no_table = document.getElementById("parcel_no_table");
   var parcel_no_label = document.getElementById("parcel_no_label");
   var TiPaPl_No = document.getElementById("TiPaPl_No");
   var case_type_label = document.getElementById("case_type_label");
   var case_type = document.getElementById("case_type");
   var crime_no_label = document.getElementById("crime_no_label");
   var crime_no = document.getElementById("crime_no");
   load_hostels();
   load_years1()
var xmlhttp;

//Disabling them on loading
v.hidden = false;
v.required = true;
year_label.hidden = true; 
  year.hidden = true; 
  year.required = false;
  year_gen_gen.hidden = true;
  year_clear.hidden = true; 
  date_range_table.hidden = true; 
  Start_date_label.hidden = true;
  start_date.hidden = true;
  start_date.required = false; 
  end_date_label.hidden = true;
  end_date.hidden = true;
  end_date.required = false;
  date_clear.hidden = true;
  date_gen.hidden = true;
  case_type_label.hidden = true;
  case_type.hidden = true;
  case_type.required = false;
  crime_no_label.hidden = true;
  crime_no.hidden = true;
  crime_no.required = false;

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("year").innerHTML=xmlhttp.responseText;
disabler();
}
}
xmlhttp.open("POST","hms_year_loader1",true);
xmlhttp.send();
}

// a function that hides either Form or Year or Terms fields wen a report type is selected.

function disabler() {
    
    
    
  var v= document.getElementById("Par_code").value;
  var year_label = document.getElementById("year_label");
  var year = document.getElementById("year");
  var year_clear = document.getElementById("year_clear");
  var year_gen_gen = document.getElementById("year_gen_gen");
  var date_range_table = document.getElementById("date_range_table");
  var Start_date_label = document.getElementById("Start_date_label");
  var start_date = document.getElementById("start_date");
   var end_date_label = document.getElementById("end_date_label");
  var end_date = document.getElementById("end_date");
  var date_clear = document.getElementById("date_clear");
  var date_gen = document.getElementById("date_gen");
  var case_type_label = document.getElementById("case_type_label");
   var case_type = document.getElementById("case_type");
   var crime_no_label = document.getElementById("crime_no_label");
   var crime_no = document.getElementById("crime_no");
   //
   var acade_year_label = document.getElementById("acade_year_label");
   var acad_year = document.getElementById("acad_year");
   var semester_year_label = document.getElementById("semester_year_label");
   var Semester = document.getElementById("Semester");
   
   
//  alert(" geek     :  "+v)
if (v == ""){
  year_label.hidden = true; 
  year.hidden = true; 
  year.required = false;
  year_gen_gen.hidden = true;
  year_clear.hidden = true; 
  date_range_table.hidden = true; 
  Start_date_label.hidden = true;
  start_date.hidden = true;
  start_date.required = false; 
  end_date_label.hidden = true;
  end_date.hidden = true;
  end_date.required = false;
  date_clear.hidden = true;
  date_gen.hidden = true; 
  case_type_label.hidden = true;
  case_type.hidden = true;
  case_type.required = false;
  crime_no_label.hidden = true;
  crime_no.hidden = true;
  crime_no.required = false;
  acade_year_label.hidden = true;
  acad_year.hidden = true;
  acad_year.required = true;
  semester_year_label.hidden = true;
  Semester.hidden = true;
  Semester.required = true;
}
if (v == 3){
  year_label.hidden = false; 
  year.hidden = false; 
  year.required = true;
  year_gen_gen.hidden = false;
  year_clear.hidden = false; 
  date_range_table.hidden = true; 
  Start_date_label.hidden = true;
  start_date.hidden = true;
  start_date.required = false; 
  end_date_label.hidden = true;
  end_date.hidden = true;
  end_date.required = false;
  date_clear.hidden = false;
  date_gen.hidden = false; 
  case_type_label.hidden = false;
  case_type.hidden = false;
  case_type.required = true;
  crime_no_label.hidden = true;
  crime_no.hidden = true;
  crime_no.required = false;
  acade_year_label.hidden = true;
  acad_year.hidden = true;
  acad_year.required = false;
  semester_year_label.hidden = true;
  Semester.hidden = true;
  Semester.required = false;
  
}
if(v==2) {
  year_label.hidden = false; 
  year.hidden = false; 
  year.required = true;
  year_gen_gen.hidden = false;
  year_clear.hidden = false; 
  date_range_table.hidden = true; 
  Start_date_label.hidden = true;
  start_date.hidden = true;
  start_date.required = false; 
  end_date_label.hidden = true;
  end_date.hidden = true;
  end_date.required = false;
  date_clear.hidden = true;
  date_gen.hidden = true;
  case_type_label.hidden = true;
  case_type.hidden = true;
  case_type.required = false;
  crime_no_label.hidden = true;
  crime_no.hidden = true;
  crime_no.required = false;
  acade_year_label.hidden = true;
  acad_year.hidden = true;
  acad_year.required = false;
  semester_year_label.hidden = true;
  Semester.hidden = true;
  Semester.required = false;
  
  
  //
  }
if(v==5 || v==6 || v==7) {
 year_label.hidden = true; 
  year.hidden = true; 
  year.required = false;
  year_gen_gen.hidden = true;
  year_clear.hidden = true; 
  date_range_table.hidden = false; 
  Start_date_label.hidden = false;
  start_date.hidden = false;
  start_date.required = true; 
  end_date_label.hidden = false;
  end_date.hidden = false;
  end_date.required = true;
  date_clear.hidden = false;
  date_gen.hidden = false;
  case_type_label.hidden = true;
  case_type.hidden = true;
  case_type.required = false;
  crime_no_label.hidden = true;
  crime_no.hidden = true;
  crime_no.required = false;
  acade_year_label.hidden = true;
  acad_year.hidden = true;
  acad_year.required = false;
  semester_year_label.hidden = true;
  Semester.hidden = true;
  Semester.required = false;
  
  }
  if(v==4) {
 year_label.hidden = true; 
  year.hidden = true; 
  year.required = false;
  year_gen_gen.hidden = false;
  year_clear.hidden = false; 
  date_range_table.hidden = true; 
  Start_date_label.hidden = true;
  start_date.hidden = true;
  start_date.required = false; 
  end_date_label.hidden = true;
  end_date.hidden = true;
  end_date.required = false;
  date_clear.hidden = true;
  date_gen.hidden = true;
  case_type_label.hidden = true;
  case_type.hidden = true;
  case_type.required = false;
  crime_no_label.hidden = false;
  crime_no.hidden = false;
  crime_no.required = true;
  acade_year_label.hidden = false;
  acad_year.hidden = false;
  acad_year.required = true;
  semester_year_label.hidden = false;
  Semester.hidden = false;
  Semester.required = true;
  
  
  }
  }

function load_hostels(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("crime_no").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","hms_hostel_loader",true);
xmlhttp.send();
}//county loader

function filter_name_item_description(acad_year){

var petrofill_type=acad_year.value; 
var xmlhttp;    
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{

document.getElementById("Semester").innerHTML= xmlhttp.responseText;;

}
}
xmlhttp.open("POST","hms_semester_loader?petrofill_type="+petrofill_type,true);
xmlhttp.send();
}
//
function load_years1(){    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("acad_year").innerHTML=xmlhttp.responseText;
load_faculties();
}
}
xmlhttp.open("POST","hms_year_loader",true);
xmlhttp.send();
}
     
        </script> 
        <SCRIPT language=Javascript>
		<!--
function isCharacterKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return true;
return false;
}

function isNumberKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return false;
return true;
}

//-->
</SCRIPT>
        <script src="jss/jquery-1.7.2.js"></script>
	<script src="jss/jquery-ui-1.10.3.custom.js"></script>
 <link href="jss/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet">
       <script src="jss/datepicker.js"></script>
       <script src="admin/ui/jquery.ui.datepicker.js"></script>
        <script>	
                $(function() {
        $( ".datepicker" ).datepicker({
                                dateFormat: "dd/mm/yy",
                                changeMonth: true,
                                changeYear: true
                               
                        });
                    
                });
            </script>        
    </head>
    <body onload="load_years();"
       
     <div id="container" style="height:auto;" >
 <%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("4")){%>    
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("1")){%>
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%>
                <div style="margin-left: 100px; margin-top: -10px; color: #000; background: #f8da4e; font-size: 20px; text-align: center;">Generate Report. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("user_added") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("user_added")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("user_added");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
                    <form action="hms_gen_report" method="post">
                        <br/>
                        <table cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:150px ;">
                            <tr>
                                <td class="align_button_left"><label for="first_name">Choose Parameter<font color="red">*</font></label></td>
                                <td class="align_button_left"><select name="Par_code" id="Par_code" class="textbox2" onchange="disabler()" required>
                                         <option value="">Choose Report Type</option>
                                        <!--<option value="1">Parcel No</option>  -->                                  
                                        <option value="2">Year Registration</option>
                                        <option value="5">Date Range Registration</option>
                                        <!--<option value="3">Successfully Booked Hostel Name</option>-->
                                        <option value="4">Successfully Booked By Hostel Name</option>
                                        <!--<option value="6">Date Range Wrangles</option>-->
                                        <!--<option value="7">Date Range Transactions</option>-->                                     
                                    </select>
                                </td>
                            </tr>
                            
                            <tr>
                           
                                <td class="align_button_left"><label id="case_type_label" for="">Case Type <font color="red">*</font></label></td>
                                <td ><select name="case_type" id="case_type" class="textbox2" >
                                         <option value="">Choose Case Type</option>
                                         <option value="1">Solved Cases</option>
                                         <option value="0">Unsolved Cases</option>
                                        
                                    </select>
                                </td>
                                
                           </tr>
                           
                           <tr>
                               <td class="align_button_left"><label id="crime_no_label" for="">Hostels <font color="red">*</font></label></td>
                                 <td ><select name="crime_no" id="crime_no" class="textbox2" >
                                         
                                        
                                    </select>
                                </td>
                           </tr><tr>

                               
                            </tr>
                            
                            <tr>
                           
                                <td class="align_button_left"><label id="year_label" for="">Choose Year <font color="red">*</font></label></td>
                                <td ><select name="year" id="year" class="textbox2" >
                                         <option value="">Choose year</option>
                                        
                                    </select>
                                </td>
                                
                           </tr>
                           
                          <tr>
                                <td class="align_button_left"><label id='acade_year_label' for="first_name">Academic Year<font color="red">*</font></label></td>
                                <td ><select name="acad_year" id="acad_year" class="textbox2" onchange="filter_name_item_description(this);" required>
                                         
                                        
                                    </select>
                                </td>
                               
                            </tr>
                      <tr>
                                <td class="align_button_left"><label id='semester_year_label' for="first_name">Semester<font color="red">*</font></label></td>
                                <td ><select name="Semester" id="Semester" class="textbox2"  required>
                                         
                                        
                                    </select>
                                </td>
                               
                            </tr>
                           <tr> 
                               <td class="align_button_left"><input id="year_clear" size="12px"  type="reset" value="clear" /></td> 
                               <td class="align_button_right"><input type="submit" id="year_gen_gen" class="submit" value="Gen' Report" onmouseover="getAge();"/></td>
                            </tr>
                        </table>
                        <table id="date_range_table" style="margin-left:100px ;">
                             <tr>
                                <td class="align_button_left"><label id="Start_date_label" for="first_name">Start Date<font color="red">*</font></label></td>
                                <td ><input id="start_date" type=text class="datepicker textbox" onclick="return checkdate();" value="" required name="start_date" student_name />
                                </td>
                                <td class="align_button_left"><label id="end_date_label" for="first_name">End Date<font color="red">*</font></label></td>
                                 <td><input id="end_date" type=text class="datepicker textbox" onclick="return checkdate();" value="" required name="end_date" student_name /></td>
                            </tr>
                            <tr> 
                               <td class="align_button_left"><input id="date_clear"  size="12px"  type="reset" value="clear" /></td> 
                               <td></td>
                               <td></td>
                               <td class="align_button_right"><input id="date_gen" type="submit" class="submit" value="Gen' Report" onmouseover="getAge();"/></td>
                            </tr>
                        </table>
                       <!-- <table id="parcel_no_table" cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:150px ;">
                           <tr>
                           
                                <td class="align_button_left"><label id="parcel_no_label" for="">Parcel NO<font color="red">*</font></label></td>
                                <td class="align_button_left"><input id="TiPaPl_No" type=text required name="TiPaPl_No" student_name class="textbox"/>
                                </td>
                                
                           </tr>
                           <tr> 
                               <td class="align_button_left"><input id="year_clear" size="12px"  type="reset" value="clear" /></td> 
                               <td class="align_button_right"><input type="submit" id="year_gen_gen" class="submit" value="Gen' Report" onmouseover="getAge();"/></td>
                            </tr>
                        </table>-->
                            
                              
    
                            
                            
                            
                            
            
                                      <%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                           
                           
                            
                       
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;"> &copy;HOSTEL MANAGEMENT SYSTEM <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>


