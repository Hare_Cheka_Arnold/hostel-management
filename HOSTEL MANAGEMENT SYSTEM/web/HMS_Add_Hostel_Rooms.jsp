<%-- 
    Document   : HMS_Add_Hostel_Rooms
    Created on : 16-Aug-2015, 10:55:29
    Author     : user
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Picture1.PNG"/>
        <title>HMS Add Rooms.</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
     <script type="text/javascript">
function load_hostels(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Hostel").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","hms_hostel_loader",true);
xmlhttp.send();
}//county loader




function filter_name_description(){

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("cat_code").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","category_name_loader",true);
xmlhttp.send();
}
//end county loader

        </script> 
        
    </head>
    <body onload="load_hostels();">
       
     <div id="container" style="height:auto;" >
 <%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%><br>
                 <div style="margin-left: 100px; margin-top: -10px; color: #000; background: #f8da4e; font-size: 20px; text-align: center;">Adding Hostel Rooms. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("Room_added") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("Room_added")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("Room_added");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
                    <form action="hms_add_room" method="post">
                        <br/>
                        <input id="hostelid" type="hidden" style="background-color: #FBE3E4"  readonly value="<%=generateRandomNumber(3941,1870888)%>" required name=hostelid class="textbox"/>
                        <table cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:150px ;">
                       
                     <tr>
                         <td class="align_button_left"><label for="first_name">Room Name<font color="red">*</font></label></td>
                                <td><input id="room_name" type=text required name="room_name"  value =""student_name class="textbox"/></td>
                               
                     </tr>
                            
                          <tr>
                                <td class="align_button_left"><label for="first_name">Hostel<font color="red">*</font></label></td>
                                <td ><select name="Hostel" id="Hostel" class="textbox2" onchange="filter_name_item_description(this);" required>
                                         
                                        
                                    </select>
                                </td>
                               
                            </tr>
                            <tr>
                         <td class="align_button_left"><label for="first_name">Maximum Occupants<font color="red">*</font></label></td>
                                <td><input id="max_occ" type=text required name="max_occ"  value =""student_name class="textbox"/></td>
                               
                     </tr>
                      <tr>
                         <td class="align_button_left"><label for="first_name">Price Per Occupant<font color="red">*</font></label></td>
                                <td><input id="priceperocc" type=text required name="priceperocc"  value =""student_name class="textbox"/></td>
                               
                     </tr>
                      <tr>
                                <td class="align_button_left"><label for="first_name">Gender<font color="red">*</font></label></td>
                                <td ><select name="gender" id="gender" class="textbox2"  required>
                                         <option value="">Choose Gender</option>
                                         <option value="M">Male</option>
                                        <option value="F">Female</option>
                                        
                                    </select>
                                </td>
                               
                            </tr>
                                                       
                                                        
                           
                                      <%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                           
                           <tr> 
                               <td class="align_button_left"><input  size="12px"  type="reset" value="clear" /></td> 
                               <td class="align_button_right"><input type="submit" class="submit" value="Add Room" onmouseover="getAge();"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;"> &copy;HOSTEL MANAGEMENT SYSTEM <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>
