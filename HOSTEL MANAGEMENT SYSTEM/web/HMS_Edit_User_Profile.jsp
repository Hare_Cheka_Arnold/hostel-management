<%-- 
    Document   : HMS_Edit_User_Profile
    Created on : Jul 19, 2015, 11:26:07 PM
    Author     : user
--%>

<%@page import="HMS_DataSource.HMS_DAtaSource"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Picture1.png"/>
        <title>HMS Editing Profile.</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 
$(function() {
$( "#datepicker" ).datepicker();
});

       function load_counties(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("county").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","county_loader",true);
xmlhttp.send();
}//county loader




function filter_districts(district){

var dist=district.value;    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    
if (dist=="")
{
//filter the districts    



document.getElementById("district").innerHTML="<option value=\"\">Choose District</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("district").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","district_loader?county_id="+dist,true);
xmlhttp.send();
}//end of filter districts     
function load_units(district){

var dist=district.value;        
var xmlhttp;    
if (dist=="")
{
document.getElementById("sp").innerHTML="<option value=\"\">Choose Supply Unit</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("sp").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","sp_loader?dist_id="+dist,true);
xmlhttp.send();
}     
        </script> 
        
    </head>
    <body onload="load_counties();">
       
     <div id="container" style="height:auto;" >
 <%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

<%
     String fname="",mname="",lname="",phone="",usern="", fname1="",  email="", username="", userlev="",crimeusername="";
     int idno=0;
//     String userid=session.getAttribute("userid").toString();
      String userid=session.getAttribute("userid").toString();
     HMS_DAtaSource conn = new HMS_DAtaSource();
//out.println("useri id  "+userid);
     String select_user_details="SELECT * FROM users WHERE user_id='"+userid+"'";
     conn.rs=conn.st.executeQuery(select_user_details);
    if(conn.rs.next()==true){
        
        
        
         phone=conn.rs.getString("phone");
         username=conn.rs.getString("username");
         userlev=conn.rs.getString("level");
         crimeusername = conn.rs.getString("fname");
//         out.println(fname);
     }
     
     

conn.rs.close();

%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%><br>
                 <div style="margin-left: 100px; margin-top: -10px; color: #000; background: #f8da4e; font-size: 20px; text-align: center;">My Profile Editing. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("user_updated") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("user_updated")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("user_updated");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
 <form action="hms_edit_profile" method="post">
     <input id="userid" type=hidden style="background-color: #FBE3E4"  readonly value="<%=userid%>" required name="userid" class="textbox"/>
      <input id="userlevel" type=hidden style="background-color: #FBE3E4"  readonly value="<%=userlev%>" required name="userlevel" class="textbox"/>
                        <br/>
                        <table cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:190px ;">
                      
    <tr>
                                                       
                            <tr>
                                <td class="align_button_left"><label for="first_name">Full Name<font color="red">*</font></label></td>
                                <td><input id="full_name" type=text required name="full_name" student_name value="<%=crimeusername%>" class="textbox"/></td></tr><tr>

                               
                            </tr>
                            
                            
                            <tr>
                                <td class="align_button_left"><label for="first_name">Phone Number<font color="red">*</font></label></td>
                                <td><input id="phone_number" type=text required name="phone_number" value="<%=phone%>" student_name class="textbox"/></td></tr><tr>

                               
                            </tr>
                           
                            <tr>
                                <td class="align_button_left"><label for="first_name">Username<font color="red">*</font></label></td>
                                <td><input id="username" type=text required name="username" student_name value="<%=username%>" class="textbox"/></td></tr><tr>

                               
                            </tr>
                            <tr>
                                 <td class="align_button_left"><label id="password_label" for="password">Password<font color="red">*</font></label></td>
                            <td><input id="password" type=password  name=pass oninput="checkPasswords()" class="textbox"/></td>

                               
                            </tr>
                            <tr>
                                 <td class="align_button_left"><label id="conf_password_label" for="conf_password">Confirm Password<font color="red">*</font></label></td>
                            <td ><input id="conf_password" type=password  name=conf_password oninput="checkPasswords()" class="textbox"/></td>
                            </tr>
                           
                            
                            
                           
                            
                            
                            
            
                                      <%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                           
                           <tr> 
                               <td class="align_button_left"><input  size="12px"  type="reset" value="clear" /></td> <td class="align_button_right">
                               <input type="submit" class="submit" value="Edit" onmouseover="getAge();"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;"> &copy HOSTEL MANAGEMENT SYSTEM <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>
