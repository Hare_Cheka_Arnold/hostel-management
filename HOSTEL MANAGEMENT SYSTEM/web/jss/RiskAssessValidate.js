/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

  $(document).ready(function(){
    	// Smart Wizard     	
  		$('#wizard').smartWizard({transitionEffect:'slideleft',onLeaveStep:leaveAStepCallback,onFinish:onFinishCallback,enableFinishButton:true});

      function leaveAStepCallback(obj){
        var step_num= obj.attr('rel');
        return validateSteps(step_num);
      }
      
      function onFinishCallback(){
       if(validateAllSteps()){
        $('form').submit();
       }
      }
            
		});
	   
    function validateAllSteps(){
       var isStepValid = true;
       
       if(validateStep1() == false){
         isStepValid = false;
         $('#wizard').smartWizard('setError',{stepnum:1,iserror:true});         
       }else{
         $('#wizard').smartWizard('setError',{stepnum:1,iserror:false});
       }
       if(validateStep2() == false){
         isStepValid = false;
         $('#wizard').smartWizard('setError',{stepnum:2,iserror:true});         
       }else{
         $('#wizard').smartWizard('setError',{stepnum:2,iserror:false});
       }
       
       if(validateStep3() == false){
         isStepValid = false;
         $('#wizard').smartWizard('setError',{stepnum:3,iserror:true});         
       }else{
         $('#wizard').smartWizard('setError',{stepnum:3,iserror:false});
       }
       if(validateStep4() == false){
         isStepValid = false;
         $('#wizard').smartWizard('setError',{stepnum:4,iserror:true});         
       }else{
         $('#wizard').smartWizard('setError',{stepnum:4,iserror:false});
       }
       
       
       if(validateStep5() == false){
         isStepValid = false;
         $('#wizard').smartWizard('setError',{stepnum:5,iserror:true});         
       }else{
         $('#wizard').smartWizard('setError',{stepnum:5,iserror:false});
       }
       
       if(!isStepValid){
          $('#wizard').smartWizard('showMessage','Please fill the marked fields in step1 and click Next');
       }
              
       return isStepValid;
    } 	
		
		
		function validateSteps(step){
		  var isStepValid = true;
      // validate step 1
      if(step == 1){
        if(validateStep1() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','Please fill the red marked fields in step'+step+ ' and click next.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
          $('#wizard').smartWizard('hideMessage');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      if(step == 2){
        if(validateStep2() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','Please fill the red marked fields in step'+step+ ' and click next.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
          $('#wizard').smartWizard('hideMessage');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      // validate step3
      if(step == 3){
        if(validateStep3() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','Please fill the  marked fields in step'+step+ ' and click next.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
          $('#wizard').smartWizard('hideMessage');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      //validate step 4
      if(step == 4){
        if(validateStep4() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','Please fill the marked fields in step'+step+ ' and click next.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
          $('#wizard').smartWizard('hideMessage');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      //validate step 5
      if(step == 5){
        if(validateStep5() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','Please fill the marked fields in step'+step+ ' and click next.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
          $('#wizard').smartWizard('hideMessage');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      return isStepValid;
    }
		
		function validateStep1(){
       var isValid = true; 
       // Validate Username
       var un = $('#DOA').val();
       if(!un && un.length <= 0){
         isValid = false;
         $('#msg_DOA').html('Please fill DOA').show();
       }else{
         $('#msg_DOA').html('').hide();
       }
       var RegPartner = $('#RegPartner').val();
       var NoRegPartner = $('#NoRegPartner').val();
       if(RegPartner === "Yes" && NoRegPartner.length <= 0 &&!NoRegPartner){
         isValid = false;
         $('#msg_DOA').html('Please fill field').show();
         document.getElementById("NoRegPartner").required="true"; 
      $("#NoRegPartner").css("border-color","#ff0000");
            $("#NoRegPartner").slideToggle( "slow", function() {});
            $("#NoRegPartner").slideToggle( "slow", function() {});
            $("#NoRegPartner").focus();
            
       }else{
         $('#msg_DOA').html('').hide();
       }
       var HIVPartStatus = $('#HIVPartStatus').val();
       var NoHIVPartStatus = $('#NoHIVPartStatus').val();
       var PartCondomUse = $('#PartCondomUse').val();
       if(HIVPartStatus === "Yes" && NoHIVPartStatus.length <= 0 && !NoHIVPartStatus){
        
         $('#msg_DOA').html('Please fill field').show();
         document.getElementById("NoHIVPartStatus").required="true"; 
         document.getElementById("PartCondomUse").required="true"; 
             $("#NoHIVPartStatus").css("border-color","#ff0000");
            $("#NoHIVPartStatus").slideToggle( "slow", function() {});
            $("#NoHIVPartStatus").slideToggle( "slow", function() {});
            $("#NoHIVPartStatus").focus();
            
            
             $("#PartCondomUse").css("border-color","#ff0000");
            $("#PartCondomUse").slideToggle( "slow", function() {});
            $("#PartCondomUse").slideToggle( "slow", function() {});
            $("#PartCondomUse").focus();
          isValid = false;
       }
       else if(HIVPartStatus === "Yes" && PartCondomUse.length <= 0 && !PartCondomUse){
        
         $('#msg_DOA').html('Please fill field').show();
         document.getElementById("NoHIVPartStatus").required="true"; 
         document.getElementById("PartCondomUse").required="true"; 
         
          isValid = false;
       }else{
         $('#msg_DOA').html('').hide();
       }

       return isValid;
    }
       function validateStep2(){
     var isValid2 = true;  
 
        var STISeekTreat = $('#STISeekTreat').val();
       var STIPlaceTreated = $('#STIPlaceTreated').val();
       if(STISeekTreat === "Yes" && STIPlaceTreated.length <= 0 &&!STIPlaceTreated){
         isValid2 = false;
         $('#msg_DOA').html('Please fill field').show();
         document.getElementById("STIPlaceTreated").required="true"; 
         
         $("#STIPlaceTreated").css("border-color","#ff0000");
            $("#STIPlaceTreated").slideToggle( "slow", function() {});
            $("#STIPlaceTreated").slideToggle( "slow", function() {});
            $("#STIPlaceTreated").focus();
       }else{
         $('#msg_DOA').html('').hide();
          document.getElementById("STIPlaceTreated").required="false";
               $("#STIPlaceTreated").css("border-color","#B0C4DE");
       }
 
       var LubricantUse = $('#LubricantUse').val();
     
        var checkboxs=document.getElementsByClassName("Lubs");
        
    var okay=false;
    for(var i=0,l=checkboxs.length;i<l;i++)
    {
      
        if(LubricantUse==="Yes" && checkboxs[i].checked==true)
        {
            okay=true;
         // alert("aaa"+checkboxs[i].checked);
        }
        else if(LubricantUse==="No" && checkboxs[i].checked==false){
           okay=true; 
       }
     else if ( LubricantUse.length <= 0 &&!LubricantUse){
              okay=true; 
         }
        
    }
    if(okay==true){
      
    }
    
    else{
        alert("Please check either KYJelly, Saliva or Other");
      isValid2 = false;
  }
  
  
   var selects=document.getElementsByClassName("LubsUse");
    var checks=false;
    for(var j=0,l=selects.length;j<l;j++)
    {
        
       
        if(LubricantUse==="Yes" && selects[j].checked===true )
        {
            checks=true;
        }
        else if(LubricantUse==="No" && selects[j].checked==false){
           checks=true; 
           
        }
        else if(LubricantUse==="Yes" && selects[j].checked==false){ 
            checks=false;
        }
        
         else if ( LubricantUse.length <= 0 &&!LubricantUse){
              checks=true; 
         }
    }
    if(checks==true){
      
    }
    
    else{ 
      alert("Please check either Anal Sex or Vaginal");
      isValid2 = false;
  }
    
  
  
      var OtherLubricants1 = $('#OtherLubricants1').val();
       var OtherLubricants = $('#OtherLubricants').val();
  if( $('#OtherLubricants1').prop('checked')===true &&  OtherLubricants.length <= 0 &&!OtherLubricants ){
        isValid2 = false;
        
            document.getElementById("OtherLubricants").required="true"; 
         alert("Specify Other");
         $("#OtherLubricants").css("border-color","#ff0000");
            $("#OtherLubricants").slideToggle( "slow", function() {});
            $("#OtherLubricants").slideToggle( "slow", function() {});
            $("#OtherLubricants").focus();
       }else{
         $('#msg_DOA').html('').hide();
          document.getElementById("OtherLubricants").required="false";
          
       }
  
  
  
        
        return isValid2;
    }  
       function validateStep3(){
     var isValid3 = true; 
 
     var UseFP = $('#UseFP').val();
       var FPMethodUsed = $('#FPMethodUsed').val();
       if(UseFP === "Yes" && FPMethodUsed.length <= 0 &&!FPMethodUsed){
         isValid3 = false;
     
         document.getElementById("FPMethodUsed").required="true"; 
         
         $("#FPMethodUsed").css("border-color","#ff0000");
            $("#FPMethodUsed").slideToggle( "slow", function() {});
            $("#FPMethodUsed").slideToggle( "slow", function() {});
            $("#FPMethodUsed").focus();
       }else{
        
          document.getElementById("FPMethodUsed").required="false";
             
       }
    
     return isValid3;
    
    }  
       function validateStep4(){
     var isValid4 = true; 
 
     var AlcoholUse = $('#AlcoholUse').val();
       var AlcoholFrequency = $('#AlcoholFrequency').val();
       if(AlcoholUse === "Yes" && AlcoholFrequency.length <= 0 &&!AlcoholFrequency){
         isValid4 = false;
            document.getElementById("AlcoholFrequency").required="true"; 
            $("#AlcoholFrequency").css("border-color","#ff0000");
            $("#AlcoholFrequency").slideToggle( "slow", function() {});
            $("#AlcoholFrequency").slideToggle( "slow", function() {});
            $("#AlcoholFrequency").focus();
       }else{
         document.getElementById("AlcoholFrequency").required="false";
                }
     var DrugUse = $('#DrugUse').val();
       var DrugType = $('#DrugType').val();
       if(DrugUse === "Yes" && (DrugType.length <= 0 &&!DrugType) ||(DrugFrequency.length <= 0 &&!DrugFrequency)){
         isValid4 = false;
            document.getElementById("DrugType").required="true"; 
            document.getElementById("DrugFrequency").required="true"; 
            $("#DrugType").css("border-color","#ff0000");
            $("#DrugType").slideToggle( "slow", function() {});
            $("#DrugType").slideToggle( "slow", function() {});
            $("#DrugType").focus();
            
            // drug frequency 
            $("#DrugFrequency").css("border-color","#ff0000");
            $("#DrugFrequency").slideToggle( "slow", function() {});
            $("#DrugFrequency").slideToggle( "slow", function() {});
            $("#DrugFrequency").focus();
       }else{
         document.getElementById("DrugType").required="false";
                }
    
    
       var VaginalDouche = $('#VaginalDouche').val();
//     alert(VaginalDouche);
        var checkboxes=document.getElementsByClassName("douche");
        
    var checker=false;
    for(var k=0,m=checkboxes.length;k<m;k++)
    {
//      alert("llll"+checkboxes[k].checked);
        if(VaginalDouche==="Yes" && checkboxes[k].checked==true)
        {
            checker=true;
         // alert("aaa"+checkboxs[i].checked);
        }
        else if(VaginalDouche==="No" && checkboxes[k].checked==false){
           checker=true; 
           
         
        } else if ( VaginalDouche.length <= 0 &&!VaginalDouche){
              checker=true; 
         }
        
      
        
    }
    if(checker===true){
      
    }
    
    else{
        alert("Please check either of the Douche Methods");
      isValid4 = false;
  }
  
  
  
  
     return isValid4;
    
    }  
       function validateStep5(){
     var isValid5 = true; 
 
 
 
  var VerbalInsults = $('#VerbalInsults').val();
       var LastVerbalInsult = $('#LastVerbalInsult').val();
       if(VerbalInsults === "Yes" && LastVerbalInsult.length <= 0 &&!LastVerbalInsult){
         isValid5 = false;
     
         document.getElementById("LastVerbalInsult").required="true"; 
         
            $("#LastVerbalInsult").css("border-color","#ff0000");
            $("#LastVerbalInsult").slideToggle( "slow", function() {});
            $("#LastVerbalInsult").slideToggle( "slow", function() {});
            $("#LastVerbalInsult").focus();
       }else{
        
          document.getElementById("LastVerbalInsult").required="false";
             
       }
  var Hit = $('#Hit').val();
       var LastVerbalInsult = $('#LastVerbalInsultHit').val();
       if(Hit === "Yes" && LastVerbalInsult.length <= 0 &&!LastVerbalInsult){
         isValid5 = false;
     
         document.getElementById("LastVerbalInsultHit").required="true"; 
         
            $("#LastVerbalInsultHit").css("border-color","#ff0000");
            $("#LastVerbalInsultHit").slideToggle( "slow", function() {});
            $("#LastVerbalInsultHit").slideToggle( "slow", function() {});
            $("#LastVerbalInsultHit").focus();
       }else{
        
          document.getElementById("LastVerbalInsultHit").required="false";
             
       }
  var ForcedSex = $('#ForcedSex').val();
       var LastVerbalInsult = $('#LastVerbalInsultForced').val();
       if(ForcedSex === "Yes" && LastVerbalInsult.length <= 0 &&!LastVerbalInsult){
         isValid5 = false;
     
         document.getElementById("LastVerbalInsultForced").required="true"; 
         
            $("#LastVerbalInsultForced").css("border-color","#ff0000");
            $("#LastVerbalInsultForced").slideToggle( "slow", function() {});
            $("#LastVerbalInsultForced").slideToggle( "slow", function() {});
            $("#LastVerbalInsultForced").focus();
       }else{
        
          document.getElementById("LastVerbalInsultForced").required="false";
             
       }
  var VerbalAbuse = $('#VerbalAbuse').val();
       var VerbalAbuseTime = $('#VerbalAbuseTime').val();
       if(VerbalAbuse === "Yes" && VerbalAbuseTime.length <= 0 &&!VerbalAbuseTime){
         isValid5 = false;
     
         document.getElementById("VerbalAbuseTime").required="true"; 
         
            $("#VerbalAbuseTime").css("border-color","#ff0000");
            $("#VerbalAbuseTime").slideToggle( "slow", function() {});
            $("#VerbalAbuseTime").slideToggle( "slow", function() {});
            $("#VerbalAbuseTime").focus();
       }else{
        
          document.getElementById("VerbalAbuseTime").required="false";
             
       }
 
  var PartnerProblem = $('#PartnerProblem').val();
       var PartnerProblemTime = $('#PartnerProblemTime').val();
       if(PartnerProblem === "Yes" && PartnerProblemTime.length <= 0 &&!PartnerProblemTime){
         isValid5 = false;
     
         document.getElementById("PartnerProblemTime").required="true"; 
         
            $("#PartnerProblemTime").css("border-color","#ff0000");
            $("#PartnerProblemTime").slideToggle( "slow", function() {});
            $("#PartnerProblemTime").slideToggle( "slow", function() {});
            $("#PartnerProblemTime").focus();
       }else{
        
          document.getElementById("PartnerProblemTime").required="false";
             
       }
       
       var SeekTreatment = $('#SeekTreatment').val();
  
       var PlaceTreated = $('#PlaceTreated').val();
    
       if(SeekTreatment === "Yes" && PlaceTreated.length <= 0 &&!PlaceTreated){
         isValid5 = false;
     
         document.getElementById("PlaceTreated").required="true"; 

         
            $("#PlaceTreated").css("border-color","#ff0000");
            $("#PlaceTreated").slideToggle( "slow", function() {});
            $("#PlaceTreated").slideToggle( "slow", function() {});
            $("#PlaceTreated").focus();
            
            $("#ReportPolice").css("border-color","#ff0000");
            $("#ReportPolice").slideToggle( "slow", function() {});
            $("#ReportPolice").slideToggle( "slow", function() {});
            $("#ReportPolice").focus();
       }
       else if(SeekTreatment.length <= 0 &&!SeekTreatment){
        
          document.getElementById("PlaceTreated").required="false";
        
             
       }
 else{
     
       document.getElementById("PlaceTreated").required="false";
          
        }
 
  
  var ReportPolice = $('#ReportPolice').val();
       var ReportWhere = $('#ReportWhere').val();
       if(ReportPolice === "Yes" && ReportWhere.length <= 0 &&!ReportWhere){
         isValid5 = false;
     
         document.getElementById("ReportWhere").required="true"; 
         
            $("#ReportWhere").css("border-color","#ff0000");
            $("#ReportWhere").slideToggle( "slow", function() {});
            $("#ReportWhere").slideToggle( "slow", function() {});
            $("#ReportWhere").focus();
       }else{
        
          document.getElementById("ReportWhere").required="false";
             
       }
        return isValid5;
    
    }  

		
	