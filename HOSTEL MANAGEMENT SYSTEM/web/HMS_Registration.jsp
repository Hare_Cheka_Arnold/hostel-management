<%-- 
    Document   : HMS_Registration
    Created on : Jul 20, 2015, 12:27:49 AM
    Author     : user
--%>

<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Picture1.png"/>
        <title>HMS REGISTRATION.</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
         
$(function() {
$( "#datepicker" ).datepicker();
});

//********************************************FUNCTION TO LOAD COUNTIES************************************
function load_dimensions(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Dimensions").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","lms_dimension_loader",true);
xmlhttp.send();
}
//county loader
//************************************************************FUNCTION TO PERFORM EVENTS DURING VOTING************************

//********************************************FUNCTION TO LOAD COUNTIES************************************
function load_faculties(){    
load_department();
// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Faculty").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","hms_faculty_loader",true);
xmlhttp.send();
}

function validateEmail(Email) {
    var email_val = Email.value;
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    alert(wewe);
    return re.test(email_val);
}
//county loader
//**********************************FILTERING DEPARTMENTS******************************************************************
function filter_departments(Faculty){
var Faculty_type=Faculty.value; 
var xmlhttp;    
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{

document.getElementById("Department").innerHTML= xmlhttp.responseText;


}
}
xmlhttp.open("POST","hms_department_loader?faculty_type="+Faculty_type,true);
xmlhttp.send();
}

function load_department(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Title_No").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","hms_title_loader",true);
xmlhttp.send();
}

//************************************************************FUNCTION TO PERFORM EVENTS DURING VOTING************************
function voting_process1(){

var dist=document.getElementById("motion_tag").value;
var yesvotes=document.getElementById("yes").value;
var novotes=document.getElementById("no").value;

var xmlhttp;    
if (dist=="")
{
document.getElementById("total_Votes").value="";
document.getElementById("yes").value="";
document.getElementById("no").value="";
document.getElementById("yes_submit").disable=true;
document.getElementById("no_submit").disable=true;
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{

var n = xmlhttp.responseText;
mit = n.split(",");

document.getElementById("yes").value=mit[1];
document.getElementById("motion_des").value=mit[6];
document.getElementById("no").value=mit[2];
document.getElementById("total_Votes").value=mit[5];
if (mit[7] == "1" ){
document.getElementById("yes_submit").disabled = true;
document.getElementById("no_submit").disabled = true;
}
else{
  document.getElementById("yes_submit").disabled = false;
document.getElementById("no_submit").disabled = false;  
}

return false;
}
}
xmlhttp.open("POST","e_voting_event_in_progress_no?dist_id="+dist+","+yesvotes+","+novotes+"",true);
xmlhttp.send();
}
function voting_process(){


var dist=document.getElementById("motion_tag").value;
var yesvotes=document.getElementById("yes").value;
var novotes=document.getElementById("no").value;

var xmlhttp;    
if (dist=="")
{
document.getElementById("total_Votes").value="";
document.getElementById("yes").value="";
document.getElementById("no").value="";
document.getElementById("yes_submit").disable=true;
document.getElementById("no_submit").disable=true;
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{

var n = xmlhttp.responseText;
mit = n.split(",");

document.getElementById("yes").value=mit[1];
document.getElementById("motion_des").value=mit[6];
document.getElementById("no").value=mit[2];
document.getElementById("total_Votes").value=mit[5];
if (mit[7] == "1" ){
document.getElementById("yes_submit").disabled = true;
document.getElementById("no_submit").disabled = true;
}
else{
  document.getElementById("yes_submit").disabled = false;
document.getElementById("no_submit").disabled = false;  
}

return false;
}
}
xmlhttp.open("POST","e_voting_event_in_progress?dist_id="+dist+","+yesvotes+","+novotes+"",true);
xmlhttp.send();
}
//************************************************************FUNCTION FOR DISABLING BUTTONS**********************************
function motion_details1(){
var dist = document.getElementById("motion_tag").value; 
   
if (dist=="")
{
document.getElementById("total_Votes").value="";
document.getElementById("yes").value="";
document.getElementById("no").value="";
document.getElementById("yes_submit").disabled=true;
document.getElementById("no_submit").disabled=true;
document.getElementById("serial_Number").value="";
//return;
}
}
//************************************************************FUNCTION FOR LOADING MOTIONS************************************
function load_motions(){    

     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("motion_tag").innerHTML=xmlhttp.responseText;
motion_details1();
}
}
xmlhttp.open("POST","evoting_motion_loader",true);
xmlhttp.send();
}


      </script> 
      <SCRIPT language=Javascript>
		<!--
function isCharacterKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return true;
return false;
}

function isNumberKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return false;
return true;
}

//-->
</SCRIPT>  
    </head>
    <body onload="load_faculties();">
       
     <div id="container" style="height:auto;" >
<%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%><br>
                 <div style="margin-left: 100px; margin-top: -10px; color: #000; background: #f8da4e; font-size: 24px; text-align: center;">Student Registration. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("Registration") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("Registration")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("Registration");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
                    <form action="hms_registration" method="post">
                        <div class="Main">
                            <div class="sub_main_one">
                                 <%
DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                                <label for="first_name" class="lbl"><em class="soso">STUDENT'S DETAILS</em><font color="red"></font></label>
                                <table cellpadding="2px" cellspacing="4px" border="0px" style="margin-left:10px ;">
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Full Names:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Full_Name"  type=text onkeypress="return isCharacterKey(event)" required ="true" name="Full_Name" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">ID NO:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="ID_NO" onkeypress="return isNumberKey(event)" type=text maxlength="8" required ="true" name="ID_NO" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Mobile NO:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                            <input id="Tel_No"  type=text required ="true" pattern="[0-9]{10,10}" maxlength="10" name="Tel_No" placeholder="07xxxxxxxx" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Email:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Email"  type=email required ="true" name="Email" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                   <!-- <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Website:</em><font color="red"></font></label>  
                                        </td>
                                        <td>
                                          <input id="Website"  type=text name="Website" student_name class="textbox"/>  
                                        </td>
                                    </tr>-->
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Registration Date:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                            <input id="Reg_Date" readonly="true" type=text  name="Reg_Date" value="<%=dateFormat.format(cal.getTime())%>" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                </table>
                                </div>
                            <div class="sub_main_two">
                                <input id="userid" type="hidden" style="background-color: #FBE3E4"  readonly value="<%=generateRandomNumber(3941,1870888)%>" required name=userid class="textbox"/>
                               <label for="first_name" class="lbl"><em class="soso">COURSE DETAILS</em><font color="red"></font></label>
                                <table cellpadding="2px" cellspacing="4px" border="0px" style="margin-left:10px ;">
                                    <tr>
                                        <td>
                                           <label for="first_name" ><em class="xoxo">Course Title:</em><font color="red">*</font></label>  
                                        </td>
                                       <td class="align_button_left"><select name="Title_No" id="Title_No" class="textbox2"  required>
                                         
                                                                            
                                    </select>
                                </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Registration NO:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Reg_No"  type=text required ="true"  name="Reg_No" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Faculty</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <select name="Faculty" id="Faculty"  class="textbox10 " onchange="filter_departments(this);" required="true">
                                         
                                        
                                    </select>  
                                        </td>
                                    </tr>
                                    
                                     <!--<tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Location:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Location"  type=text required="true" name="Location" student_name class="textbox"/>  
                                        </td>
                                    </tr>-->
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Department</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <select name="Department" id="Department"  class="textbox10 " required>
                                             <option value="">Choose Department</option>
                                        
                                    </select>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo"> Year of Course</em><font color="red">*</font></label>  
                                        </td>
                                        <td class="align_button_left"><select name="Course_Year" id="Course_Year" class="textbox2"  required>
                                         <option value="">Course Year</option>
                                                                        
                                        <option value="Year One">Year One</option>
                                        <option value="Year Two">Year Two</option>                                        
                                        <option value="Year Three">Year Three</option>
                                        <option value="Year Four">Year Four</option>                                        
                                        <option value="Year Five">Year Five</option>
                                                                            
                                    </select>
                                </td>
                                       
                                    </tr>
                                    
                                </table>
                            </div>
                            <table cellpadding="0px" cellspacing="4px" border="0px" style="margin-left:110px ;">
                            <tr> 
                               <td class="align_button_left">
                                   <input  size="12px"  type="reset" value="clear" />
                               </td> 
                               
                            
                               <td class="align_button_right">
                               <input type="submit" class="submit end_ryt" value="Register" onmouseover="getAge();"/>
                               </td>
                               
                            </tr>
                            </table>
                            <div style="clear: both">
                                
                            </div>
                        </div>
                                     
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;">&copy HOSTEL MANAGEMENT SYSTEM  <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>
