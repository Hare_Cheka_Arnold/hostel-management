-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2015 at 03:27 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cs_hostel_management`
--
CREATE DATABASE IF NOT EXISTS `cs_hostel_management` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cs_hostel_management`;

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

CREATE TABLE IF NOT EXISTS `audit` (
  `action_id` int(50) NOT NULL AUTO_INCREMENT,
  `action` varchar(200) NOT NULL,
  `time` varchar(100) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `host_comp` text NOT NULL,
  PRIMARY KEY (`action_id`),
  KEY `actor_id` (`actor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

-- --------------------------------------------------------

--
-- Table structure for table `e_voting_stop_login_date`
--

CREATE TABLE IF NOT EXISTS `e_voting_stop_login_date` (
  `Date_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Actual_Date` varchar(200) NOT NULL,
  `Determiner` varchar(200) NOT NULL,
  PRIMARY KEY (`Date_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `e_voting_stop_login_date`
--

INSERT INTO `e_voting_stop_login_date` (`Date_ID`, `Actual_Date`, `Determiner`) VALUES
(1, 'Aug-30-2015 10:02:10 PM', 'ed3b0f1e00a67acb17c637545322056b');

-- --------------------------------------------------------

--
-- Table structure for table `hms_book_master`
--

CREATE TABLE IF NOT EXISTS `hms_book_master` (
  `Hostel_ID` int(5) NOT NULL,
  `Price` double NOT NULL DEFAULT '0',
  `Gender` varchar(3) NOT NULL,
  `Hostel_Unique_ID` int(200) NOT NULL,
  `Booking_ID` int(5) NOT NULL AUTO_INCREMENT,
  `Room_Name` varchar(200) NOT NULL,
  `Maximum_Occupants` int(5) NOT NULL DEFAULT '0',
  `Booked_Occupants` int(5) NOT NULL DEFAULT '0',
  `Booking_Status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0=Not Existing 1 = Existing',
  `Academic Year` varchar(20) NOT NULL DEFAULT 'None',
  `Semester_Type` tinyint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Booking_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `hms_book_master_record`
--

CREATE TABLE IF NOT EXISTS `hms_book_master_record` (
  `Master_Record_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Occupant_Full_Names` varchar(200) NOT NULL,
  `Occupants_UserID` int(30) NOT NULL,
  `Booking_ID` int(11) NOT NULL,
  `Semester` tinyint(3) DEFAULT '0',
  `Academic_Year` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Master_Record_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `hms_charm`
--

CREATE TABLE IF NOT EXISTS `hms_charm` (
  `Date_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Actual_Date` varchar(200) NOT NULL,
  `Determiner` varchar(200) NOT NULL,
  PRIMARY KEY (`Date_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hms_department`
--

CREATE TABLE IF NOT EXISTS `hms_department` (
  `Department_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Department_Name` varchar(200) NOT NULL,
  `Faculty_ID` varchar(11) NOT NULL,
  PRIMARY KEY (`Department_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `hms_faculty`
--

CREATE TABLE IF NOT EXISTS `hms_faculty` (
  `Faculty_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Faculty_Name` varchar(200) NOT NULL,
  `Faculty_Description` text NOT NULL,
  PRIMARY KEY (`Faculty_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `hms_hostels`
--

CREATE TABLE IF NOT EXISTS `hms_hostels` (
  `Hostels_ID` int(5) NOT NULL AUTO_INCREMENT,
  `Hostels_Name` varchar(200) NOT NULL,
  `Hostel_Description` text NOT NULL,
  PRIMARY KEY (`Hostels_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `hms_registration`
--

CREATE TABLE IF NOT EXISTS `hms_registration` (
  `Full_Name` varchar(200) NOT NULL,
  `ID_Number` varchar(200) NOT NULL,
  `Tel_NO` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `Reg_Date` varchar(200) NOT NULL,
  `Title_No` varchar(200) NOT NULL,
  `Reg_No` varchar(200) NOT NULL,
  `Faculty` tinyint(3) NOT NULL,
  `Department` int(6) NOT NULL,
  `Course_Year` text NOT NULL,
  PRIMARY KEY (`Reg_No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hms_reg_emp_record`
--

CREATE TABLE IF NOT EXISTS `hms_reg_emp_record` (
  `Reg_Emp` varchar(200) NOT NULL,
  `User_ID` varchar(30) NOT NULL,
  PRIMARY KEY (`Reg_Emp`,`User_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hms_semesters`
--

CREATE TABLE IF NOT EXISTS `hms_semesters` (
  `Semester_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Semester_Name` varchar(200) NOT NULL,
  `Year_ID` int(11) NOT NULL,
  PRIMARY KEY (`Semester_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fname`, `mname`, `lname`, `phone`, `username`, `password`, `level`, `timestamp`) VALUES
(231477, 'Juno Okello', NULL, NULL, '0714568932', 'Janet', '9cded2b3b1d9c8e102c70f48cd131a49', '4', '2015-07-01 05:57:41'),
(384749, 'Programmer Hare Cheka Arnold', NULL, NULL, '0717212724', 'Cheka', 'ed3b0f1e00a67acb17c637545322056b', '10', '2015-03-28 09:43:18');

-- --------------------------------------------------------

--
-- Table structure for table `year1`
--

CREATE TABLE IF NOT EXISTS `year1` (
  `Year_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Year_name` varchar(200) NOT NULL,
  PRIMARY KEY (`Year_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `year1`
--

INSERT INTO `year1` (`Year_ID`, `Year_name`) VALUES
(1, '2010'),
(2, '2011'),
(3, '2012'),
(4, '2013'),
(5, '2014'),
(6, '2015');

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE IF NOT EXISTS `years` (
  `Year_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Semesters` varchar(200) DEFAULT NULL,
  `Status` tinyint(3) NOT NULL DEFAULT '1',
  `Year_name` varchar(200) NOT NULL,
  PRIMARY KEY (`Year_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
