/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class hms_edit_hostel extends HttpServlet {
//Initialising variables
    HttpSession session;
    String Hostel_Name, Hostel_Description, Hostel_ID,current_time;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        
         //Getting session
        session=request.getSession();
        
        //Creating a new object
        HMS_DAtaSource conn = new HMS_DAtaSource();
        /*Receiving parameters from the form*/
         Hostel_Name = request.getParameter("hostel_name");
         Hostel_Description = request.getParameter("hostel_des");
         Hostel_ID = request.getParameter("Hostel_ID");
         
         System.out.println("Hostel Name= "+Hostel_Name);
System.out.println("Hostel Description= "+Hostel_Description);
System.out.println("Hostel ID= "+Hostel_ID);
         
         /*The query that populate the database*/
         String DBupdate = "UPDATE hms_hostels SET Hostels_Name = '"+Hostel_Name+"',Hostel_Description = '"+Hostel_Description+"'";
         
         String DBupdate1 = "UPDATE hms_hostels SET Hostels_Name = '"+Hostel_Name+"'";
         
         //Query to populate the Audit table
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int date = cal.get(Calendar.DATE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);
        String yr, mnth, dater, hr, mn, sc, action = "";
        yr = Integer.toString(year);
        mnth = Integer.toString(month);
        dater = Integer.toString(date);
        hr = Integer.toString(hour);
        mn = Integer.toString(min);
        sc = Integer.toString(sec);
        
        current_time = yr + "-" + mnth + "-" + dater + "-" + hr + ":" + mn + ":" + sc;
        
        String Action = "Edited by "+session.getAttribute("fullname").toString()+"";
        
        //Getting ip of comp
        String ip = InetAddress.getLocalHost().getHostAddress();
        
        String action_recorded = "INSERT INTO audit (action,time,actor_id,host_comp) "
                + "VALUES ('" + Action + "','" + current_time + "','" + session.getAttribute("userid").toString() + "','" + session.getAttribute("computer_name").toString() + " " + ip + "')";
        
         
         if (Hostel_Description.isEmpty() || Hostel_Description==null){
             
             /*Executing the Queries*/
             conn.st1.executeUpdate(DBupdate1);
             
             //Executing to Audit Query
                conn.st2.executeUpdate(action_recorded);
                
                //Creating a session
                session.setAttribute("hostel_edit", "<font color=\"green\">Hostel "+Hostel_Name+" Edited succesfully</font>");
         
         }
         else{
         conn.st3.executeUpdate(DBupdate);
         
         conn.st2.executeUpdate(action_recorded);
         
         //Creating a session
         session.setAttribute("hostel_edit", "<font color=\"green\">Hostel "+Hostel_Name+" Edited succesfully</font>");
         }
         
         response.sendRedirect("HMS_Editing_Hostel.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(hms_edit_hostel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(hms_edit_hostel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
