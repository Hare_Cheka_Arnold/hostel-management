/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class hms_booking_room extends HttpServlet {

    //Initialising the variables
   
    HttpSession session;
    String room_name,room_gender,max_occ,book_occ,room_price,receipt_No,Master_booking_id,userid,acad_year,Semester;
    int Result_occ;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
 //*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            room_name = request.getParameter("room_name");
            room_gender = request.getParameter("room_gender");
            max_occ = request.getParameter("max_occ");
            book_occ = request.getParameter("book_occ");
            room_price = request.getParameter("room_price");
            receipt_No = request.getParameter("receipt_No");
            Master_booking_id = request.getParameter("Master_booking_id");
            userid = request.getParameter("userid");
            Semester = request.getParameter("Semester");
            acad_year = request.getParameter("acad_year");
            
            //Creating an object
            HMS_DAtaSource conn = new HMS_DAtaSource();
            
            //Displaying them on console
            System.out.println("Room Name ="+room_name);
            System.out.println("Room Gender ="+room_gender);
            System.out.println("Max Occ ="+max_occ);
            System.out.println("Booked Occ ="+book_occ);
            System.out.println("Room Prices ="+room_price);
            System.out.println("Hostel Unique ID ="+Master_booking_id);
            System.out.println("User ID ="+userid);
            
            
            //Checking if the row does exist
            String checking_existing = "SELECT * FROM hms_book_master_record WHERE Occupants_UserID = '"+userid+"' AND "
                    + "Semester = '"+Semester+"' AND Academic_Year = '"+acad_year+"'";
            
            conn.rs6 = conn.st6.executeQuery(checking_existing);
            
            if (!conn.rs6.next()){
            
            //Doing the calculaton
            Result_occ = get_number_occupants(room_name) + 1;
            //Initialising the Queries
            String Updating_room_table = "UPDATE hms_book_master SET Booked_Occupants = '"+Result_occ+"' WHERE Booking_ID "
            + "= '"+room_name+"'";
            
            String Updating_room_table1 = "UPDATE hms_book_master SET Booking_Status = '"+1+"'";
            
            //Updating the master record with Occupants details
            String Insert_Master = "INSERT INTO hms_book_master_record (Occupant_Full_Names,Occupants_UserID"
                    + ",Booking_ID,Semester,Academic_Year) VALUES ('"+get_fullnames(userid)+"','"+userid+"','"+Master_booking_id+"',"
                    + "'"+Semester+"','"+acad_year+"')";
            
            conn.st1.executeUpdate(Updating_room_table);
            
            if (get_number_occupants_after(room_name) == get_number_occupants_max(room_name)){
            //Executing the query
             conn.st2.executeUpdate(Updating_room_table1);   
            }
            
            //Executing the last query
            conn.st3.executeUpdate(Insert_Master);
                    
            //Setting session
            session.setAttribute("No_Room", "<font color=\"green\">"+get_fullnames(userid)+"You have booked room  <b>"+get_roomName(room_name)+"</b>succesfully</font>");
            
            //Redirecting to the next page
            response.sendRedirect("HMS_Search_Booking.jsp");
            }
            else{
            //Setting session
            session.setAttribute("booking_room_added", "<font color=\"red\">"+get_fullnames(userid)+" you already registered for semester "+getsemnem(Semester)+" and year "+getyearnem(acad_year)+"</font>");
            
            //Redirecting to the next page
            response.sendRedirect("HMS_Add_Booking.jsp");
            }
        
    }

  
    
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(hms_booking_room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(hms_booking_room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//Getting the number of booked occupants before updating
    public int get_number_occupants(String Room_ID) throws SQLException{
int occ_no = 0;
String get_no = "SELECT * FROM hms_book_master WHERE Booking_ID = '"+Room_ID+"'";

HMS_DAtaSource conn = new HMS_DAtaSource();

conn.rs1 = conn.st1.executeQuery(get_no);
while(conn.rs1.next()){
occ_no = conn.rs1.getInt("Booked_Occupants");
}
return occ_no;
}
    
    //Getting the number of booked occupants after updating
    public int get_number_occupants_after(String Room_ID) throws SQLException{
    int occ_no = 0;
String get_no = "SELECT * FROM hms_book_master WHERE Booking_ID = '"+Room_ID+"'";

HMS_DAtaSource conn = new HMS_DAtaSource();

conn.rs2 = conn.st2.executeQuery(get_no);
while(conn.rs2.next()){
occ_no = conn.rs2.getInt("Booked_Occupants");
}
return occ_no;
    }
    
    //Getting the maximum of Occupants
     public int get_number_occupants_max(String Room_ID) throws SQLException{
    int occ_no = 0;
String get_no = "SELECT * FROM hms_book_master WHERE Booking_ID = '"+Room_ID+"'";

HMS_DAtaSource conn = new HMS_DAtaSource();

conn.rs3 = conn.st3.executeQuery(get_no);

while(conn.rs3.next()){
occ_no = conn.rs3.getInt("Maximum_Occupants");
}
return occ_no;
    }
     
     //Getting fullnames of bookee
     public String get_fullnames(String UserID) throws SQLException{
     String Full_names = "";
     
     HMS_DAtaSource connn = new HMS_DAtaSource();
     String Geting_name = "SELECT * FROM users WHERE user_id = '"+UserID+"'";
     
     connn.rs4 = connn.st4.executeQuery(Geting_name);
     while (connn.rs4.next()){
     Full_names = connn.rs4.getString("fname");
     }
     
     return Full_names;
     }
     
     //Getting room name
     public String get_roomName(String RoomID) throws SQLException{
     String RoomNem = "";
     
     //Creating object
    HMS_DAtaSource connn = new HMS_DAtaSource();
     
     String getroom_name = "SELECT * FROM hms_book_master WHERE Booking_ID = '"+RoomID+"'";
     connn.rs5 = connn.st5.executeQuery(getroom_name);
     while (connn.rs5.next()){
     RoomNem = connn.rs5.getString("Room_Name");
     }
     return RoomNem;
     }
     
     //GEtting semester name
     public String getsemnem(String Semester_ID) throws SQLException{
     String Semenem = "";
     
     HMS_DAtaSource connn = new HMS_DAtaSource();
     String Getsem = "SELECT * FROM hms_semesters WHERE Semester_ID = '"+Semester_ID+"'";
     
     connn.rs1 = connn.st1.executeQuery(Getsem);
     
     while (connn.rs1.next()){
     Semenem = connn.rs1.getString("Semester_Name");
     }
     return Semenem;
     }
     
     public String getyearnem(String Semester_ID) throws SQLException{
     String Semenem = "";
     
     HMS_DAtaSource connn = new HMS_DAtaSource();
     String Getsem = "SELECT * FROM years WHERE Year_ID = '"+Semester_ID+"'";
     
     connn.rs1 = connn.st1.executeQuery(Getsem);
     
     while (connn.rs1.next()){
     Semenem = connn.rs1.getString("Year_name");
     }
     return Semenem;
     }
}
