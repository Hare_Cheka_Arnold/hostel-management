/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
public class hms_semester_loader extends HttpServlet {

    //Initialises the variables
String petrofill_type_result,petrofill_type,petrofill_categ_name;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
         //Executes the try function
        try {
            //Creatig db connection
            HMS_DAtaSource conn = new HMS_DAtaSource();
           //Receiving the parameters
           petrofill_type=request.getParameter("petrofill_type");
           petrofill_categ_name = "";
           //Getting the name of the Category
           String Categ_name = "SELECT * FROM years WHERE Year_ID = '"+petrofill_type+"'";
           conn.rs1=conn.st1.executeQuery(Categ_name);
           while(conn.rs1.next()){
           petrofill_categ_name = conn.rs1.getString("Year_name");
           }
           System.out.println(petrofill_categ_name);
           //Declares the variable to null
          petrofill_type_result="<option value=\"\">Choose "+petrofill_categ_name+" Semester</option>";
          //SQL query being executed.
           String district_selector="SELECT * FROM hms_semesters WHERE Year_ID='"+petrofill_type+"'";
           conn.rs=conn.st.executeQuery(district_selector);
           while(conn.rs.next()){
            petrofill_type_result+= "<option value=\""+conn.rs.getString("Semester_ID")+"\">"+conn.rs.getString("Semester_Name")+"</option>"; 
           
           System.out.println(conn.rs.getString(2));
           System.out.println(conn.rs.getString(3));
           System.out.println(petrofill_type_result);
           }
if(conn.rs!=null){
            conn.rs.close();
            }
if(conn.st!=null){
            conn.st.close();
            }
            out.println("<html>");
            out.println("<head>");
            out.println("<title></title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>,"+petrofill_type_result+ ",</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    try {
        processRequest(request, response);
    } catch (SQLException ex) {
        Logger.getLogger(hms_semester_loader.class.getName()).log(Level.SEVERE, null, ex);
    }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    try {
        processRequest(request, response);
    } catch (SQLException ex) {
        Logger.getLogger(hms_semester_loader.class.getName()).log(Level.SEVERE, null, ex);
    }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
