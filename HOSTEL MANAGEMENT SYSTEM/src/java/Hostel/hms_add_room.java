/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class hms_add_room extends HttpServlet {

   //Initialising the variables
   
    HttpSession session;
    String hostelid,room_name,Hostel,max_occ,gender,price;
      boolean statuz = false;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");

//*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            hostelid = request.getParameter("hostelid");           
            room_name = request.getParameter("room_name");
            Hostel = request.getParameter("Hostel");           
            max_occ = request.getParameter("max_occ");
            gender = request.getParameter("gender");
            price = request.getParameter("priceperocc");
            
            //Creating a new object for the data source
            HMS_DAtaSource conn = new HMS_DAtaSource();
            
            //Initialising the query to insert new users.                      

            String save_dept = "INSERT INTO hms_book_master (Hostel_ID,Room_Name,Maximum_Occupants,Hostel_Unique_ID,Gender,price) "
                    + "VALUES ('" + Hostel + "','" + room_name + "','" + max_occ + "','" + hostelid + "','" + gender + "',"
                    + "'" + price + "')";
            
            //Initialising the query to check if the user exists

            String Dept_checker = "SELECT * FROM hms_book_master WHERE Room_Name='" + room_name + "' && Hostel_ID='" + Hostel + "'";

            String query_hostel_name = "SELECT * FROM hms_hostels WHERE Hostels_ID = '"+Hostel+"'";

            conn.rs1 = conn.st1.executeQuery(query_hostel_name);
            String hostel_fetched ="";
            while(conn.rs1.next()){
             hostel_fetched = conn.rs1.getString("Hostels_Name");
            }
            //Statement to execute the query to check
            conn.rs = conn.st.executeQuery(Dept_checker);

            //check if ward is already registered 
            if (!conn.rs.next()) {
                
                //if ward does not exist execute the query to insert new ward
                conn.st1.executeUpdate(save_dept);

                

                session.setAttribute("Room_added", "<font color=\"green\">Room "+room_name+" of Hostel "+hostel_fetched+" added succesfully</font>");
            } else {
                session.setAttribute("Room_added", "<b><font color=\"red\">Sorry, Room "+room_name+" of Hostel "+hostel_fetched+" is already registered.</font></b>");


            }
            
            response.sendRedirect("HMS_Add_Hostel_Rooms.jsp");

        } catch (SQLException ex) {
            Logger.getLogger(hms_add_room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
