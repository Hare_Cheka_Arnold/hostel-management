/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class hms_hostel_booking_room extends HttpServlet {

   //Initialise Variables
    String Hostel_Name,Lease_Activity;
    int Lease_Count = 0;
    
    //Initialise session
    private HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
         //Initialising the session
        session = request.getSession();
        
        //Receiving the parameter
        Hostel_Name = request.getParameter("hostel_name");
        System.out.println("Lease Count Before check ="+Lease_Count);
        
         //Initiating hostel activity to null
        Lease_Activity = "";
        
        //Creating a new object for database connection
        HMS_DAtaSource conn = new HMS_DAtaSource();
        
        //Initialising the query
        String Fetch_Land_Activity = "SELECT * FROM hms_hostels WHERE Hostels_Name LIKE '%"+Hostel_Name+"%'";
        
        
        
        String Hostel_ID = "";
        
        //Executing the query
        conn.rs=conn.st.executeQuery(Fetch_Land_Activity);
        
        //Executing if exist any records
        while(conn.rs.next()){
            
            //Assigning the Hostel_ID
            Hostel_ID = conn.rs.getString("Hostels_ID");
        
        }
        
     //Initialising Query
        String Fetch_Hostels_Room = "SELECT * FROM hms_book_master WHERE Hostel_ID = '"+Hostel_ID+"' AND "
                + "Booking_Status = '"+1+"'";
        
        //Executing the query
        conn.rs1=conn.st1.executeQuery(Fetch_Hostels_Room);
        
        while (conn.rs1.next()){
        
        //Getting the details
            Lease_Activity +=""+conn.rs1.getString("Room_Name")+"|"+conn.rs1.getString("Maximum_Occupants")+"|"
                    + ""+conn.rs1.getString("Price")+"|"+conn.rs1.getString("Gender")+"|"+conn.rs1.getString("Booked_Occupants")+"|"+conn.rs1.getString("Booking_ID")+"|"
                    + ""+conn.rs1.getString("Hostel_Unique_ID")+"|";
            
            Lease_Count ++;        
        System.out.println("Rooms count ="+Lease_Count);
        }
        int count_pos = ((7*Lease_Count)+2);
        String Room_Activity = "";
        Room_Activity +=""+count_pos+"|"+Lease_Activity+"";
        System.out.println("Rooms count after check ="+Lease_Count);
        
        
        //Checking the count
        if (Lease_Count == 0){
            
        session.setAttribute("No_Room", "<font color=\"red\">Hostel "+Hostel_Name+" Has No Available Rooms</font>");
        }
        Room_Activity += ""+Lease_Count+"";
        
        System.out.println("The String ="+Room_Activity);
        //Closing Connections
        if(conn.rs!=null){
            conn.rs.close();
            }
        if(conn.st!=null){
            conn.st.close();
            }
            out.println("<html>");
            out.println("<head>");
            out.println("<title></title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>|"+Room_Activity+"|</h1>");
            out.println("</body>");
            out.println("</html>");
        
       Lease_Count = 0;
       System.out.println("After depositint = "+Lease_Count);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(hms_hostel_booking_room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(hms_hostel_booking_room.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
