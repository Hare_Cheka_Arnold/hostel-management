/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class HMS_rectification extends HttpServlet {

    //Initialising the variables
   
    HttpSession session;
    String Billing_Stop_Date,pass, County_Description;
      boolean statuz = false;
      MessageDigest m;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NoSuchAlgorithmException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            response.setContentType("text/html;charset=UTF-8");

//*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Billing_Stop_Date = request.getParameter("Billing_Stop_Date");
            pass = request.getParameter("pass");
            
             m = MessageDigest.getInstance("MD5");
        m.update(pass.getBytes(), 0, pass.length());
        String pw = new BigInteger(1, m.digest()).toString(16);
            
            //Creating a new object for the data source
            HMS_DAtaSource conn = new HMS_DAtaSource();
            
            //Accessing to check the password
            //query for checking user existance in the database
        String select1 = "SELECT * FROM e_voting_stop_login_date WHERE Date_ID = '"+1+"'";
        
    conn.rs = conn.st.executeQuery(select1);
        
          while (conn.rs.next()) {  
              if (conn.rs.getString("Determiner").equals(pw)){
            //Initialising the query to insert new counties.                      

            String save_county = "UPDATE e_voting_stop_login_date SET Actual_Date='"+Billing_Stop_Date+"' WHERE Date_ID='"+1+"'";
         
            //Check if the billing date is in the correct format
           Date date = null;
try {
    date = new SimpleDateFormat("MMM-dd-yyyy hh:mm:ss a").parse(Billing_Stop_Date);
} catch (ParseException ex) {
    ex.printStackTrace();
}
if (date == null || !(session.getAttribute("userid").toString().equalsIgnoreCase("384749"))) {
    session.setAttribute("Administrator_Billing", "<b><font color=\"red\">Sorry, System Stop Date "+Billing_Stop_Date+" was not registered.</font></b>");

} else {
    conn.st1.executeUpdate(save_county);
     session.setAttribute("Administrator_Billing", "<font color=\"green\">System Stop Date "+Billing_Stop_Date+" registered succesfully</font>");
            
} 
            
         }
              else{
              session.setAttribute("Administrator_Billing", "<b><font color=\"red\">Sorry, Wrong Password Contact Super Admin.</font></b>");
            
              }   
            response.sendRedirect("HMS_Billing_Interface.jsp");
              
          }

        } catch (SQLException ex) {
            Logger.getLogger(HMS_rectification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HMS_rectification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HMS_rectification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
