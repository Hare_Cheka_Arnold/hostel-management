/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class hms_registration extends HttpServlet {

     MessageDigest m;
     HttpSession session;
    
    String Fullname, ID_NO, Tel_NO, Email, Reg_Date, userid, Title_No, Reg_No, Faculty, Department, Course_Year,current_time;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NoSuchAlgorithmException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        
        //Creates an object for db connection
        HMS_DAtaSource conn = new HMS_DAtaSource();
        
        //Receiving parameters from Form
        Fullname = request.getParameter("Full_Name");
        ID_NO = request.getParameter("ID_NO");
        Tel_NO = request.getParameter("Tel_No");
        Email = request.getParameter("Email");
        Reg_Date = request.getParameter("Reg_Date");
        userid = request.getParameter("userid");
        Title_No = request.getParameter("Title_No");
        Reg_No = request.getParameter("Reg_No");
        Faculty = request.getParameter("Faculty");
        Department = request.getParameter("Department");
        Course_Year = request.getParameter("Course_Year");
        
        //**************************************Queries that manipulate the Db on table Registration****************
        //Query to check if the Reg Number Exists
        String checker = "SELECT * FROM hms_registration WHERE Reg_No ='" + Reg_No + "' ";
        
        //Query to save Students Details
        String Hostel_Details_Registration = "INSERT INTO hms_registration (Full_Name,ID_Number,Tel_NO,Email,Reg_Date,"
                + "Title_No,Reg_No,Faculty,Department,Course_Year) "
                    + "VALUES ('" + Fullname + "','" + ID_NO + "','" + Tel_NO + "','" + Email + "','" + Reg_Date + "',"
                + "'" + Title_No + "','" + Reg_No + "','" + Faculty + "','" + Department + "','" + Course_Year + "')";
        
         //encrypt password
        String Both_Names = "Undergraduate "+ Fullname;

        m = MessageDigest.getInstance("MD5");
        m.update(ID_NO.getBytes(), 0, ID_NO.length());
        String pw = new BigInteger(1, m.digest()).toString(16);
        
        String Hostel_User_Saving ="insert into users (user_id,username,fname,phone,password,level,lname) "
                    + "values ('" + userid + "','" + Reg_No + "','" + Both_Names + "','" + Tel_NO + "','" + pw + "','" + 3 + "','" + 1 + "')";
            
        
        //Query to populate the Audit table
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int date = cal.get(Calendar.DATE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);
        String yr, mnth, dater, hr, mn, sc, action = "";
        yr = Integer.toString(year);
        mnth = Integer.toString(month);
        dater = Integer.toString(date);
        hr = Integer.toString(hour);
        mn = Integer.toString(min);
        sc = Integer.toString(sec);
        
        current_time = yr + "-" + mnth + "-" + dater + "-" + hr + ":" + mn + ":" + sc;
        
        String Action = "Registration by "+Fullname+"";
        
        //Getting ip of comp
        String ip = InetAddress.getLocalHost().getHostAddress();
        
        String action_recorded = "INSERT INTO audit (action,time,actor_id,host_comp) "
                + "VALUES ('" + Action + "','" + current_time + "','" + userid + "','" + ip + "')";
        
//****************************************Queries to be executed*******************************************
        //Executing the query to check
         conn.rs = conn.st.executeQuery(checker);
         session = request.getSession();
         if (!conn.rs.next()) {
                
                //if user does not exist execute the query to insert new user
                conn.st1.executeUpdate(Hostel_Details_Registration);
                
                //Executing to Audit Query
                conn.st2.executeUpdate(action_recorded);
                        
                conn.st3.executeUpdate(Hostel_User_Saving);

                session.setAttribute("error_login", "<font color=\"green\">Registration of "+Fullname+" and Reg NO "+Reg_No+" was succesfully</font>");
                
            } else {
                session.setAttribute("error_login", "<b><font color=\"red\">Sorry, Registration of "+Fullname+" and Reg NO "+Reg_No+" already exists.</font></b>");

            }
         
         //Redirects the system the registration page
        response.sendRedirect("index.jsp");
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         try {
             try {
                 processRequest(request, response);
             } catch (SQLException ex) {
                 Logger.getLogger(hms_registration.class.getName()).log(Level.SEVERE, null, ex);
             }
         } catch (NoSuchAlgorithmException ex) {
             Logger.getLogger(hms_registration.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         try {
             try {
                 processRequest(request, response);
             } catch (SQLException ex) {
                 Logger.getLogger(hms_registration.class.getName()).log(Level.SEVERE, null, ex);
             }
         } catch (NoSuchAlgorithmException ex) {
             Logger.getLogger(hms_registration.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
