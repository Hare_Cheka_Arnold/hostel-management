/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 *
 * @author user
 */
public class hms_gen_report extends HttpServlet {

    //Initiating the variables
    HttpSession session;
    
    String parameter,case_type,crime_no,year,start_date,end_date,year_reg,hostel,acad_year,Semester;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
       //Getting session
        session=request.getSession();
        
        //Initialising the db connection
        HMS_DAtaSource conn = new HMS_DAtaSource();
        
        //Receiving the parameters from the form
        parameter = request.getParameter("Par_code");
        
//************************************************************1st E-Crime Report(Year)*************************************
        if (parameter.equals("2")){
            //Receiving the parameters from the form required to generate this report
            year_reg = request.getParameter("year");
        
            //Showing to console
            System.out.println("Chosen year registration = "+year_reg);
            
            //Creating an Excel workbook
            HSSFWorkbook wb=new HSSFWorkbook();
                
            //Creating the worksheet
            HSSFSheet shet1=wb.createSheet();
        
            //Initialising the variable row starter
            int row_starter = 3;
            //Initialising the variable column starter
            int column_starter = 2;
        
            //**********************************************Styles for the created work Book****************************************
            //Code showing the font of the styles
            HSSFFont font_header=wb.createFont();
            font_header.setFontHeightInPoints((short)10);
            font_header.setFontName("Arial Black");
            //font.setItalic(true);
            font_header.setBoldweight((short)15);
            font_header.setColor(HSSFColor.BLACK.index);
        
            //Cell Style known as "style_header"
            CellStyle style_header=wb.createCellStyle();
            style_header.setFont(font_header);
            style_header.setWrapText(true);
            style_header.setFillForegroundColor(HSSFColor.DARK_YELLOW.index);
            style_header.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
 
            style_header.setBorderBottom(CellStyle.BORDER_THIN);
            style_header.setBorderLeft(CellStyle.BORDER_THIN);
            style_header.setBorderRight(CellStyle.BORDER_THIN);
            style_header.setBorderTop(CellStyle.BORDER_THIN);
            style_header.setAlignment(CellStyle.ALIGN_CENTER);
        
        
            //Cell Style header known as "indicator_style"
            CellStyle indicator_style = wb.createCellStyle();
            indicator_style.setFillForegroundColor(HSSFColor.BROWN.index);
            indicator_style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            HSSFFont fonts = wb.createFont();
            fonts.setColor(HSSFColor.BLACK.index);
            fonts.setFontHeightInPoints((short) 17);
            indicator_style.setFont(fonts);
            indicator_style.setBorderBottom(CellStyle.BORDER_THIN);
            indicator_style.setBorderLeft(CellStyle.BORDER_THIN);
            indicator_style.setBorderRight(CellStyle.BORDER_THIN);
            indicator_style.setBorderTop(CellStyle.BORDER_THIN);
            indicator_style.setAlignment(CellStyle.ALIGN_CENTER);
        
            //Style known as "cell_style"
            CellStyle cell_style = wb.createCellStyle();
            HSSFFont fonts1 = wb.createFont();
            fonts1.setColor(HSSFColor.BLACK.index);
            fonts1.setFontHeightInPoints((short) 11);
            cell_style.setWrapText(true);
            cell_style.setFont(fonts1);
            cell_style.setBorderBottom(CellStyle.BORDER_THIN);
            cell_style.setBorderLeft(CellStyle.BORDER_THIN);
            cell_style.setBorderRight(CellStyle.BORDER_THIN);
            cell_style.setBorderTop(CellStyle.BORDER_THIN);
            //****************************************************Finishing with styling in the workbook****************************** 
            //Initialising the row starters
            HSSFRow rw2=shet1.createRow(row_starter - 3);
            HSSFRow rw3=shet1.createRow(row_starter - 2);
            HSSFRow rw4=shet1.createRow(row_starter - 1);
        
            //Creating the cell for the title
            HSSFCell cell1 = rw2.createCell(column_starter);    
            cell1.setCellValue("HOSTEL MANAGEMENT SYSTEM REPORT");
            cell1.setCellStyle(indicator_style);
            //Setting the column widths
            shet1.setColumnWidth(2, 3000 );
            
            //Creating a loop for the title cells that concatenate
        for( int m=3;m<=9;m++) {
            //Creating the next cell for the title
        HSSFCell cell22 = rw2.createCell(m);    
        cell22.setCellValue("");
        cell22.setCellStyle(indicator_style);
        shet1.setColumnWidth(m, 3000 );
        
        }
        
        //Merging the cells of the year
        shet1.addMergedRegion(new CellRangeAddress(0,0,2,9));
        
        
                //This shows the year
        //Creating the cell for the title
        HSSFCell cell2 = rw3.createCell(column_starter);    
        cell2.setCellValue(""+year_reg+" HMS REGISTRATION REPORTS");
        cell2.setCellStyle(style_header);
        //Setting the column widths
        shet1.setColumnWidth(2, 2600 );
        shet1.setColumnWidth(3, 4000 );
        
        for( int m=3;m<=9;m++) {
            //Creating the next cell for the title
        HSSFCell cell3 = rw3.createCell(m);    
        cell3.setCellValue("");
        cell3.setCellStyle(indicator_style);
        shet1.setColumnWidth(m, 3000 );
        
        }
        
         //Merging the cells of the report type
        shet1.addMergedRegion(new CellRangeAddress(1,1,2,9));
        
        //Writing the topics
        //Creating the column types
        
        HSSFCell cell4 = rw4.createCell(2);    
        cell4.setCellValue("FULL NAME");
        cell4.setCellStyle(style_header);
        shet1.setColumnWidth(2, 4500 );
        
        HSSFCell cell5 = rw4.createCell(3);    
        cell5.setCellValue("ID NUMBER");
        cell5.setCellStyle(style_header);
        shet1.setColumnWidth(3, 4500 );
        
        HSSFCell cell9 = rw4.createCell(4);    
        cell9.setCellValue("TELEPHONE NUMBER");
        cell9.setCellStyle(style_header);
        shet1.setColumnWidth(4, 3500 );
        
        HSSFCell cell6 = rw4.createCell(5);    
        cell6.setCellValue("EMAIL");
        cell6.setCellStyle(style_header);
        shet1.setColumnWidth(5, 6500 );
        
        HSSFCell cell7 = rw4.createCell(6);    
        cell7.setCellValue("COURSE TITLE");
        cell7.setCellStyle(style_header);
        shet1.setColumnWidth(6, 6500 );
        
        HSSFCell cell8 = rw4.createCell(7);    
        cell8.setCellValue("FACULTY" );
        cell8.setCellStyle(style_header);
        shet1.setColumnWidth(7, 6500 );
        
        HSSFCell cell10 = rw4.createCell(8);    
        cell10.setCellValue("DEPARTMENT");
        cell10.setCellStyle(style_header);
        shet1.setColumnWidth(8, 6500 );
        
        HSSFCell cell11 = rw4.createCell(9);    
        cell11.setCellValue("REG NO");
        cell11.setCellStyle(style_header);
        shet1.setColumnWidth(9, 3500 );
        
        //Getting the body to excel
        int body_row = 3;
        int body_column = 2;
        
        //The Main Query
        String ecrime_year_report = "SELECT * FROM hms_registration"
                + " WHERE "
                + "Reg_Date LIKE '%" + year_reg + "%'";
        
        //Prints out the query
          System.out.println(ecrime_year_report);
          
          //Executes the query
        conn.rs=conn.st.executeQuery(ecrime_year_report);
        
        while(conn.rs.next()){
        
        ///Writing to cell
        HSSFRow rw5=shet1.createRow(body_row);
        //Writing the data to Excel
            HSSFCell cell19 = rw5.createCell(2);    
             cell19.setCellValue(conn.rs.getString("Full_Name"));
             cell19.setCellStyle(cell_style);
             
             HSSFCell cell20 = rw5.createCell(3);    
             cell20.setCellValue(conn.rs.getString("ID_Number"));
             cell20.setCellStyle(cell_style);
             
             HSSFCell cell21 = rw5.createCell(4);    
             cell21.setCellValue(conn.rs.getString("Tel_NO"));
             cell21.setCellStyle(cell_style);
             
             HSSFCell cell22 = rw5.createCell(5);    
             cell22.setCellValue(conn.rs.getString("Email"));
             cell22.setCellStyle(cell_style);
             
             HSSFCell cell23 = rw5.createCell(6);    
             cell23.setCellValue(conn.rs.getString("Title_No"));
             cell23.setCellStyle(cell_style);
             
             HSSFCell cell24 = rw5.createCell(7);    
             cell24.setCellValue(getdiscdate(conn.rs.getString("Faculty")));
             cell24.setCellStyle(cell_style);
             
             HSSFCell cell25 = rw5.createCell(8);    
             cell25.setCellValue(get_officer_name(conn.rs.getInt("Department")));
             cell25.setCellStyle(cell_style);
             
             HSSFCell cell26 = rw5.createCell(9);    
             cell26.setCellValue(conn.rs.getString("Reg_No"));
             cell26.setCellStyle(cell_style);
        
        body_row ++;
        }
        
            
                              // write it as an excel attachment
ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
wb.write(outByteStream);
byte [] outArray = outByteStream.toByteArray();
response.setContentType("application/ms-excel");
response.setContentLength(outArray.length);
response.setHeader("Expires:", "0"); // eliminates browser caching
response.setHeader("Content-Disposition", "attachment; filename="+year_reg+"_HMS_Registration_Report.xls");
OutputStream outStream = response.getOutputStream();
outStream.write(outArray);
outStream.flush();
        
        }
       else if (parameter.equals("5")){
            //Receiving the parameters from the form required to generate this report
            //Receiving the parameters from the form required to generate this report
        year_reg = request.getParameter("year");
        start_date = request.getParameter("start_date");
        end_date = request.getParameter("end_date");
        
            //Showing to console
            System.out.println("Chosen year registration = "+year_reg);
            
            //Creating an Excel workbook
            HSSFWorkbook wb=new HSSFWorkbook();
                
            //Creating the worksheet
            HSSFSheet shet1=wb.createSheet();
        
            //Initialising the variable row starter
            int row_starter = 3;
            //Initialising the variable column starter
            int column_starter = 2;
        
            //**********************************************Styles for the created work Book****************************************
            //Code showing the font of the styles
            HSSFFont font_header=wb.createFont();
            font_header.setFontHeightInPoints((short)10);
            font_header.setFontName("Arial Black");
            //font.setItalic(true);
            font_header.setBoldweight((short)15);
            font_header.setColor(HSSFColor.BLACK.index);
        
            //Cell Style known as "style_header"
            CellStyle style_header=wb.createCellStyle();
            style_header.setFont(font_header);
            style_header.setWrapText(true);
            style_header.setFillForegroundColor(HSSFColor.DARK_YELLOW.index);
            style_header.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
 
            style_header.setBorderBottom(CellStyle.BORDER_THIN);
            style_header.setBorderLeft(CellStyle.BORDER_THIN);
            style_header.setBorderRight(CellStyle.BORDER_THIN);
            style_header.setBorderTop(CellStyle.BORDER_THIN);
            style_header.setAlignment(CellStyle.ALIGN_CENTER);
        
        
            //Cell Style header known as "indicator_style"
            CellStyle indicator_style = wb.createCellStyle();
            indicator_style.setFillForegroundColor(HSSFColor.BROWN.index);
            indicator_style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            HSSFFont fonts = wb.createFont();
            fonts.setColor(HSSFColor.BLACK.index);
            fonts.setFontHeightInPoints((short) 17);
            indicator_style.setFont(fonts);
            indicator_style.setBorderBottom(CellStyle.BORDER_THIN);
            indicator_style.setBorderLeft(CellStyle.BORDER_THIN);
            indicator_style.setBorderRight(CellStyle.BORDER_THIN);
            indicator_style.setBorderTop(CellStyle.BORDER_THIN);
            indicator_style.setAlignment(CellStyle.ALIGN_CENTER);
        
            //Style known as "cell_style"
            CellStyle cell_style = wb.createCellStyle();
            HSSFFont fonts1 = wb.createFont();
            fonts1.setColor(HSSFColor.BLACK.index);
            fonts1.setFontHeightInPoints((short) 11);
            cell_style.setWrapText(true);
            cell_style.setFont(fonts1);
            cell_style.setBorderBottom(CellStyle.BORDER_THIN);
            cell_style.setBorderLeft(CellStyle.BORDER_THIN);
            cell_style.setBorderRight(CellStyle.BORDER_THIN);
            cell_style.setBorderTop(CellStyle.BORDER_THIN);
            //****************************************************Finishing with styling in the workbook****************************** 
            //Initialising the row starters
            HSSFRow rw2=shet1.createRow(row_starter - 3);
            HSSFRow rw3=shet1.createRow(row_starter - 2);
            HSSFRow rw4=shet1.createRow(row_starter - 1);
        
            //Creating the cell for the title
            HSSFCell cell1 = rw2.createCell(column_starter);    
            cell1.setCellValue("HOSTEL MANAGEMENT SYSTEM REPORT");
            cell1.setCellStyle(indicator_style);
            //Setting the column widths
            shet1.setColumnWidth(2, 3000 );
            
            //Creating a loop for the title cells that concatenate
        for( int m=3;m<=9;m++) {
            //Creating the next cell for the title
        HSSFCell cell22 = rw2.createCell(m);    
        cell22.setCellValue("");
        cell22.setCellStyle(indicator_style);
        shet1.setColumnWidth(m, 3000 );
        
        }
        
        //Merging the cells of the year
        shet1.addMergedRegion(new CellRangeAddress(0,0,2,9));
        
        
                //This shows the year
        //Creating the cell for the title
        HSSFCell cell2 = rw3.createCell(column_starter);    
        cell2.setCellValue("FROM "+start_date+" TO "+end_date+" HMS REGISTRATION REPORTS");
        cell2.setCellStyle(style_header);
        //Setting the column widths
        shet1.setColumnWidth(2, 2600 );
        shet1.setColumnWidth(3, 4000 );
        
        for( int m=3;m<=9;m++) {
            //Creating the next cell for the title
        HSSFCell cell3 = rw3.createCell(m);    
        cell3.setCellValue("");
        cell3.setCellStyle(indicator_style);
        shet1.setColumnWidth(m, 3000 );
        
        }
        
         //Merging the cells of the report type
        shet1.addMergedRegion(new CellRangeAddress(1,1,2,9));
        
        //Writing the topics
        //Creating the column types
        
        HSSFCell cell4 = rw4.createCell(2);    
        cell4.setCellValue("FULL NAME");
        cell4.setCellStyle(style_header);
        shet1.setColumnWidth(2, 4500 );
        
        HSSFCell cell5 = rw4.createCell(3);    
        cell5.setCellValue("ID NUMBER");
        cell5.setCellStyle(style_header);
        shet1.setColumnWidth(3, 4500 );
        
        HSSFCell cell9 = rw4.createCell(4);    
        cell9.setCellValue("TELEPHONE NUMBER");
        cell9.setCellStyle(style_header);
        shet1.setColumnWidth(4, 3500 );
        
        HSSFCell cell6 = rw4.createCell(5);    
        cell6.setCellValue("EMAIL");
        cell6.setCellStyle(style_header);
        shet1.setColumnWidth(5, 6500 );
        
        HSSFCell cell7 = rw4.createCell(6);    
        cell7.setCellValue("COURSE TITLE");
        cell7.setCellStyle(style_header);
        shet1.setColumnWidth(6, 6500 );
        
        HSSFCell cell8 = rw4.createCell(7);    
        cell8.setCellValue("FACULTY" );
        cell8.setCellStyle(style_header);
        shet1.setColumnWidth(7, 6500 );
        
        HSSFCell cell10 = rw4.createCell(8);    
        cell10.setCellValue("DEPARTMENT");
        cell10.setCellStyle(style_header);
        shet1.setColumnWidth(8, 6500 );
        
        HSSFCell cell11 = rw4.createCell(9);    
        cell11.setCellValue("REG NO");
        cell11.setCellStyle(style_header);
        shet1.setColumnWidth(9, 3500 );
        
        //Getting the body to excel
        int body_row = 3;
        int body_column = 2;
        
        //The Main Query
        String ecrime_year_report = "SELECT * FROM hms_registration "
                + " WHERE "
                + "(STR_TO_DATE(Reg_Date,'%e/%c/%Y')) BETWEEN (STR_TO_DATE('"+start_date+"','%e/%c/%Y')) AND (STR_TO_DATE('"+end_date+"','%e/%c/%Y'))";
        
        //Prints out the query
          System.out.println(ecrime_year_report);
          
          //Executes the query
        conn.rs=conn.st.executeQuery(ecrime_year_report);
        
        while(conn.rs.next()){
        
        ///Writing to cell
        HSSFRow rw5=shet1.createRow(body_row);
        //Writing the data to Excel
            HSSFCell cell19 = rw5.createCell(2);    
             cell19.setCellValue(conn.rs.getString("Full_Name"));
             cell19.setCellStyle(cell_style);
             
             HSSFCell cell20 = rw5.createCell(3);    
             cell20.setCellValue(conn.rs.getString("ID_Number"));
             cell20.setCellStyle(cell_style);
             
             HSSFCell cell21 = rw5.createCell(4);    
             cell21.setCellValue(conn.rs.getString("Tel_NO"));
             cell21.setCellStyle(cell_style);
             
             HSSFCell cell22 = rw5.createCell(5);    
             cell22.setCellValue(conn.rs.getString("Email"));
             cell22.setCellStyle(cell_style);
             
             HSSFCell cell23 = rw5.createCell(6);    
             cell23.setCellValue(conn.rs.getString("Title_No"));
             cell23.setCellStyle(cell_style);
             
             HSSFCell cell24 = rw5.createCell(7);    
             cell24.setCellValue(getdiscdate(conn.rs.getString("Faculty")));
             cell24.setCellStyle(cell_style);
             
             HSSFCell cell25 = rw5.createCell(8);    
             cell25.setCellValue(get_officer_name(conn.rs.getInt("Department")));
             cell25.setCellStyle(cell_style);
             
             HSSFCell cell26 = rw5.createCell(9);    
             cell26.setCellValue(conn.rs.getString("Reg_No"));
             cell26.setCellStyle(cell_style);
        
        body_row ++;
        }
        
            
                              // write it as an excel attachment
ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
wb.write(outByteStream);
byte [] outArray = outByteStream.toByteArray();
response.setContentType("application/ms-excel");
response.setContentLength(outArray.length);
response.setHeader("Expires:", "0"); // eliminates browser caching
response.setHeader("Content-Disposition", "attachment; filename="+start_date+"_TO_"+end_date+"_HMS_Report.xls");
OutputStream outStream = response.getOutputStream();
outStream.write(outArray);
outStream.flush();
        
        } 
        
        else if (parameter.equals("4")){
            //Receiving the parameters from the form required to generate this report
            //Receiving the parameters from the form required to generate this report
        hostel = request.getParameter("crime_no");
        acad_year = request.getParameter("acad_year");
        Semester = request.getParameter("Semester");
        
        
            //Showing to console
            System.out.println("Chosen Hostel = "+hostel);
            
            //Creating an Excel workbook
            HSSFWorkbook wb=new HSSFWorkbook();
                
            //Creating the worksheet
            HSSFSheet shet1=wb.createSheet();
        
            //Initialising the variable row starter
            int row_starter = 3;
            //Initialising the variable column starter
            int column_starter = 2;
        
            //**********************************************Styles for the created work Book****************************************
            //Code showing the font of the styles
            HSSFFont font_header=wb.createFont();
            font_header.setFontHeightInPoints((short)10);
            font_header.setFontName("Arial Black");
            //font.setItalic(true);
            font_header.setBoldweight((short)15);
            font_header.setColor(HSSFColor.BLACK.index);
        
            //Cell Style known as "style_header"
            CellStyle style_header=wb.createCellStyle();
            style_header.setFont(font_header);
            style_header.setWrapText(true);
            style_header.setFillForegroundColor(HSSFColor.DARK_YELLOW.index);
            style_header.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
 
            style_header.setBorderBottom(CellStyle.BORDER_THIN);
            style_header.setBorderLeft(CellStyle.BORDER_THIN);
            style_header.setBorderRight(CellStyle.BORDER_THIN);
            style_header.setBorderTop(CellStyle.BORDER_THIN);
            style_header.setAlignment(CellStyle.ALIGN_CENTER);
        
        
            //Cell Style header known as "indicator_style"
            CellStyle indicator_style = wb.createCellStyle();
            indicator_style.setFillForegroundColor(HSSFColor.BROWN.index);
            indicator_style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            HSSFFont fonts = wb.createFont();
            fonts.setColor(HSSFColor.BLACK.index);
            fonts.setFontHeightInPoints((short) 17);
            indicator_style.setFont(fonts);
            indicator_style.setBorderBottom(CellStyle.BORDER_THIN);
            indicator_style.setBorderLeft(CellStyle.BORDER_THIN);
            indicator_style.setBorderRight(CellStyle.BORDER_THIN);
            indicator_style.setBorderTop(CellStyle.BORDER_THIN);
            indicator_style.setAlignment(CellStyle.ALIGN_CENTER);
        
            //Style known as "cell_style"
            CellStyle cell_style = wb.createCellStyle();
            HSSFFont fonts1 = wb.createFont();
            fonts1.setColor(HSSFColor.BLACK.index);
            fonts1.setFontHeightInPoints((short) 11);
            cell_style.setWrapText(true);
            cell_style.setFont(fonts1);
            cell_style.setBorderBottom(CellStyle.BORDER_THIN);
            cell_style.setBorderLeft(CellStyle.BORDER_THIN);
            cell_style.setBorderRight(CellStyle.BORDER_THIN);
            cell_style.setBorderTop(CellStyle.BORDER_THIN);
            //****************************************************Finishing with styling in the workbook****************************** 
            //Initialising the row starters
            HSSFRow rw2=shet1.createRow(row_starter - 3);
            HSSFRow rw3=shet1.createRow(row_starter - 2);
            HSSFRow rw4=shet1.createRow(row_starter - 1);
        
            //Creating the cell for the title
            HSSFCell cell1 = rw2.createCell(column_starter);    
            cell1.setCellValue("HOSTEL MANAGEMENT SYSTEM REPORT");
            cell1.setCellStyle(indicator_style);
            //Setting the column widths
            shet1.setColumnWidth(2, 3000 );
            
            //Creating a loop for the title cells that concatenate
        for( int m=3;m<=9;m++) {
            //Creating the next cell for the title
        HSSFCell cell22 = rw2.createCell(m);    
        cell22.setCellValue("");
        cell22.setCellStyle(indicator_style);
        shet1.setColumnWidth(m, 3000 );
        
        }
        
        //Merging the cells of the year
        shet1.addMergedRegion(new CellRangeAddress(0,0,2,9));
        
        
                //This shows the year
        //Creating the cell for the title
        HSSFCell cell2 = rw3.createCell(column_starter);    
        cell2.setCellValue(""+get_hostel_name(hostel)+" "+get_Semester_name(Semester,acad_year)+" YEAR "+get_Academin_name(acad_year)+" SUCCESSFULLY HMS REGISTERED");
        cell2.setCellStyle(style_header);
        //Setting the column widths
        shet1.setColumnWidth(2, 2600 );
        shet1.setColumnWidth(3, 4000 );
        
        for( int m=3;m<=9;m++) {
            //Creating the next cell for the title
        HSSFCell cell3 = rw3.createCell(m);    
        cell3.setCellValue("");
        cell3.setCellStyle(indicator_style);
        shet1.setColumnWidth(m, 3000 );
        
        }
        
         //Merging the cells of the report type
        shet1.addMergedRegion(new CellRangeAddress(1,1,2,9));
        
        //Writing the topics
        //Creating the column types
        
        HSSFCell cell4 = rw4.createCell(2);    
        cell4.setCellValue("FULL NAME");
        cell4.setCellStyle(style_header);
        shet1.setColumnWidth(2, 4500 );
        
        HSSFCell cell5 = rw4.createCell(3);    
        cell5.setCellValue("ID NUMBER");
        cell5.setCellStyle(style_header);
        shet1.setColumnWidth(3, 4500 );
        
        HSSFCell cell9 = rw4.createCell(4);    
        cell9.setCellValue("TELEPHONE NUMBER");
        cell9.setCellStyle(style_header);
        shet1.setColumnWidth(4, 3500 );
        
        HSSFCell cell6 = rw4.createCell(5);    
        cell6.setCellValue("EMAIL");
        cell6.setCellStyle(style_header);
        shet1.setColumnWidth(5, 6500 );
        
        HSSFCell cell7 = rw4.createCell(6);    
        cell7.setCellValue("COURSE TITLE");
        cell7.setCellStyle(style_header);
        shet1.setColumnWidth(6, 6500 );
        
        HSSFCell cell8 = rw4.createCell(7);    
        cell8.setCellValue("FACULTY" );
        cell8.setCellStyle(style_header);
        shet1.setColumnWidth(7, 6500 );
        
        HSSFCell cell10 = rw4.createCell(8);    
        cell10.setCellValue("DEPARTMENT");
        cell10.setCellStyle(style_header);
        shet1.setColumnWidth(8, 6500 );
        
        HSSFCell cell11 = rw4.createCell(9);    
        cell11.setCellValue("REG NO");
        cell11.setCellStyle(style_header);
        shet1.setColumnWidth(9, 3500 );
        
        //Getting the body to excel
        int body_row = 3;
        int body_column = 2;
        
        //The Main Query
        String ecrime_year_report = "SELECT * FROM hms_registration as registra, hms_book_master as room, "
                + "hms_book_master_record as records, users as users WHERE room.Hostel_ID = '"+hostel+"'"
                + " AND room.Hostel_Unique_ID = records.Booking_ID AND users.user_id = records.Occupants_UserID"
                + " AND users.phone = registra.Tel_NO AND records.Semester = '"+Semester+"' AND records.Academic_Year"
                + " = '"+acad_year+"'";
                
        //Prints out the query
          System.out.println(ecrime_year_report);
          
          //Executes the query
        conn.rs=conn.st.executeQuery(ecrime_year_report);
        
        while(conn.rs.next()){
        
        ///Writing to cell
        HSSFRow rw5=shet1.createRow(body_row);
        //Writing the data to Excel
            HSSFCell cell19 = rw5.createCell(2);    
             cell19.setCellValue(conn.rs.getString("Full_Name"));
             cell19.setCellStyle(cell_style);
             
             HSSFCell cell20 = rw5.createCell(3);    
             cell20.setCellValue(conn.rs.getString("ID_Number"));
             cell20.setCellStyle(cell_style);
             
             HSSFCell cell21 = rw5.createCell(4);    
             cell21.setCellValue(conn.rs.getString("Tel_NO"));
             cell21.setCellStyle(cell_style);
             
             HSSFCell cell22 = rw5.createCell(5);    
             cell22.setCellValue(conn.rs.getString("Email"));
             cell22.setCellStyle(cell_style);
             
             HSSFCell cell23 = rw5.createCell(6);    
             cell23.setCellValue(conn.rs.getString("Title_No"));
             cell23.setCellStyle(cell_style);
             
             HSSFCell cell24 = rw5.createCell(7);    
             cell24.setCellValue(getdiscdate(conn.rs.getString("Faculty")));
             cell24.setCellStyle(cell_style);
             
             HSSFCell cell25 = rw5.createCell(8);    
             cell25.setCellValue(get_officer_name(conn.rs.getInt("Department")));
             cell25.setCellStyle(cell_style);
             
             HSSFCell cell26 = rw5.createCell(9);    
             cell26.setCellValue(conn.rs.getString("Reg_No"));
             cell26.setCellStyle(cell_style);
        
        body_row ++;
        }
        
            
                              // write it as an excel attachment
ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
wb.write(outByteStream);
byte [] outArray = outByteStream.toByteArray();
response.setContentType("application/ms-excel");
response.setContentLength(outArray.length);
response.setHeader("Expires:", "0"); // eliminates browser caching
response.setHeader("Content-Disposition", "attachment; filename="+get_hostel_name(hostel)+"Successfuls_HMS_Report.xls");
OutputStream outStream = response.getOutputStream();
outStream.write(outArray);
outStream.flush();
        
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(hms_gen_report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(hms_gen_report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
 public String getdiscdate(String CrimeNo) throws SQLException{
    String DiscpDate = "";
    
    //Creating an Object
    HMS_DAtaSource con = new HMS_DAtaSource();
    
    //Initialising the QUery
    String GetDis = "SELECT * FROM hms_faculty WHERE Faculty_ID = '"+CrimeNo+"'";
    
    //Executing the Query
    con.rs3 = con.st3.executeQuery(GetDis);
    
    //Executing the while loop
    while(con.rs3.next()){
    DiscpDate = con.rs3.getString("Faculty_Name");
    }
        return DiscpDate;
    
            
    }
 public String get_officer_name (int officer_ID) throws SQLException{
     String officer_name = "";
        //Creating an object
        HMS_DAtaSource connn = new HMS_DAtaSource();
        
        //Initialising the query
        String Get_Category = "SELECT * FROM hms_department WHERE Department_ID = '"+officer_ID+"'";
        
        /*Executing the Query*/
        connn.rs2 = connn.st2.executeQuery(Get_Category);
        
        //Accessing the while loop
        while(connn.rs2.next()){
        officer_name = connn.rs2.getString("Department_Name");
        }
       return officer_name;
     }
 
 public String get_hostel_name (String officer_ID) throws SQLException{
     String officer_name = "";
        //Creating an object
        HMS_DAtaSource connn = new HMS_DAtaSource();
        
        //Initialising the query
        String Get_Category = "SELECT * FROM hms_hostels WHERE Hostels_ID = '"+officer_ID+"'";
        
        /*Executing the Query*/
        connn.rs2 = connn.st2.executeQuery(Get_Category);
        
        //Accessing the while loop
        while(connn.rs2.next()){
        officer_name = connn.rs2.getString("Hostels_Name");
        }
       return officer_name;
     }
 
 public String get_Academin_name (String officer_ID) throws SQLException{
     String officer_name = "";
        //Creating an object
        HMS_DAtaSource connn = new HMS_DAtaSource();
        
        //Initialising the query
        String Get_Category = "SELECT * FROM years WHERE Year_ID = '"+officer_ID+"'";
        
        /*Executing the Query*/
        connn.rs2 = connn.st2.executeQuery(Get_Category);
        
        //Accessing the while loop
        while(connn.rs2.next()){
        officer_name = connn.rs2.getString("Year_name");
        }
        connn.rs2.close();
        connn.st2.close();
       return officer_name;
     }
 
 public String get_Semester_name (String Sem,String Acade) throws SQLException{
     String officer_name = "";
        //Creating an object
        HMS_DAtaSource connn = new HMS_DAtaSource();
        
        //Initialising the query
        String Get_Category = "SELECT * FROM hms_semesters WHERE Semester_ID = '"+Sem+"' AND Year_ID "
                + "='"+Acade+"'";
        
        /*Executing the Query*/
        connn.rs2 = connn.st2.executeQuery(Get_Category);
        
        //Accessing the while loop
        while(connn.rs2.next()){
        officer_name = connn.rs2.getString("Semester_Name");
        }
        connn.rs2.close();
        connn.st2.close();
       return officer_name;
     }
}
