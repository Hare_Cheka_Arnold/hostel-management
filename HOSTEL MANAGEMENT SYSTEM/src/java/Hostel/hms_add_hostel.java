/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class hms_add_hostel extends HttpServlet {

    /*Initialising variables*/
    HttpSession session;
    String Hostel_Name, Hostel_Desc;
      boolean statuz = false;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            response.setContentType("text/html;charset=UTF-8");

//*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Hostel_Name = request.getParameter("hostel_name");
            Hostel_Desc = request.getParameter("hostel_des");
            
            
            //Creating a new object for the data source
            HMS_DAtaSource conn = new HMS_DAtaSource();
            
            //Initialising the query to insert new counties.                      

            String save_county = "INSERT INTO hms_hostels (Hostels_Name,Hostel_Description) "
                    + "VALUES ('" + Hostel_Name + "','" + Hostel_Desc + "')";
            
            //When description is empty or null
            String save_county1 = "INSERT INTO hms_hostels (Hostels_Name) "
                    + "VALUES ('" + Hostel_Name + "')";
            
            //Initialising the query to check if the county exists

            String county_checker = "SELECT * FROM hms_hostels WHERE Hostels_Name='" + Hostel_Name + "'";


            //Statement to execute the query to check
            conn.rs = conn.st.executeQuery(county_checker);

            //check if ward is already registered 
            if (!conn.rs.next()) {
                
                if (!(Hostel_Desc == null || Hostel_Desc.equalsIgnoreCase("null") || Hostel_Desc.isEmpty())){
                //if county does not exist and county description is not null execute the query to insert new couty
                conn.st1.executeUpdate(save_county);
                }
                else{
                    //if county does not exist and county description is null execute the query to insert new couty
                    conn.st1.executeUpdate(save_county1);
                }
                

                session.setAttribute("Hostel_added", "<font color=\"green\">Hostel "+Hostel_Name+" added succesfully</font>");
            } else {
                session.setAttribute("Hostel_added", "<b><font color=\"red\">Sorry, Hostel "+Hostel_Desc+" is already registered.</font></b>");


            }
            
            response.sendRedirect("HMS_Add_Hostel.jsp");

        } catch (SQLException ex) {
            Logger.getLogger(hms_add_hostel.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
