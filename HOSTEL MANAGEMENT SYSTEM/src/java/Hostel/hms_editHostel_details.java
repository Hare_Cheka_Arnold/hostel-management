/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hostel;

import HMS_DataSource.HMS_DAtaSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author user
 */
@WebServlet(name = "hms_editHostel_details", urlPatterns = {"/hms_editHostel_details"})
public class hms_editHostel_details extends HttpServlet {

   //Initialising Variables
   String hostel_reg_ID;
   
   //Initialising Session
    private HttpSession session;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        //Getting session
        session = request.getSession();
        
        //Creating a new object database connection
        HMS_DAtaSource conn = new HMS_DAtaSource();
        
        /*Receiving parameters of the passed form*/
        hostel_reg_ID = request.getParameter("land_regID");
        
        //Writing a Query to get the entire details
        String Getting_Hostel_Details = "SELECT * FROM hms_hostels WHERE Hostels_ID = '"+hostel_reg_ID+"'";
        
        //Initiating the connection
        conn.rs = conn.st.executeQuery(Getting_Hostel_Details);
        
        //Creating a JSON
        JSONObject Hostel_Entire_Details = new JSONObject();
        
        //Executing the while statement 
        while (conn.rs.next()){
        Hostel_Entire_Details.accumulate("Hostel_Name", conn.rs.getString("Hostels_Name"));
        Hostel_Entire_Details.accumulate("Hostel_Description", conn.rs.getString("Hostel_Description"));
        Hostel_Entire_Details.accumulate("Hostel_ID", hostel_reg_ID);
       

        }
        
        //Setting the accumulated JSON to session
        session.setAttribute("Accumulated_details", Hostel_Entire_Details);
        
        //Redirecting to the editing page
        response.sendRedirect("HMS_Edit_Hostel_Details.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           processRequest(request, response);
       } catch (SQLException ex) {
           Logger.getLogger(hms_editHostel_details.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           processRequest(request, response);
       } catch (SQLException ex) {
           Logger.getLogger(hms_editHostel_details.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
